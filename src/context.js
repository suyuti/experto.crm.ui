import React, { Component } from 'react'
import { withRouter } from 'react-router';

const ExpertoContext = React.createContext();

export default class _ExpertoProvider extends Component {
    state = {
        user: JSON.parse(localStorage.getItem('user')),
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.user !== prevState.user) {
            if (this.state.user === undefined) {
                localStorage.removeItem('user')
            }
            else {
                localStorage.setItem('user', JSON.stringify(this.state.user))
            }
        }
    }
    render() {
        return (
            <ExpertoContext.Provider
            value = {{
                ...this.state
            }}>
                {this.props.children}
            </ExpertoContext.Provider>
        )
    }
}

const ExpertoConsumer = ExpertoContext.Consumer;
const ExpertoProvider = withRouter(_ExpertoProvider)
export {
    ExpertoProvider,
    ExpertoConsumer
}
import { http } from './axios';

function getAll() {
    return http.get("/api/user")
}

function getById(id) {
    return http.get(`/api/user/${id}`)
}

function create(user) {
    return http.post('/api/user', user)
}

function update(user) {
    return http.put(`/api/user/${user._id}`, user)
}

function remove(user) {
    return http.delete( `/api/user/${user._id}`)
}

 

export const userService = {
    getAll,
    getById,
    create,
    update,
    remove, 
}; 

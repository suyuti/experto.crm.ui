import { http } from './axios'

function getAll() {
    return http.get("/api/osb")
}

function getById(id) {
    return http.get(`/api/osb/${id}`)
}

function create(osb) {
    return http.post('/api/osb', osb)
}

function update(osb) {
    return http.put(`/api/osb/${osb._id}`, osb)
}

function remove(osb) {
    return http.delete( `/api/osb/${osb._id}`)
}

export const osbService = {
    getAll,
    getById,
    create,
    update,
    remove
};


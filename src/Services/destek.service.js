import { http } from './axios'

function getAll() {
    return http.get("/api/destek")
}

function getById(id) {
    return http.get(`/api/destek/${id}`)
}

function create(destek) {
    return http.post('/api/destek', destek)
}

function update(destek) {
    return http.put(`/api/destek/${destek._id}`, destek)
}

function remove(destek) {
    return http.delete( `/api/destek/${destek._id}`)
}

export const destekService = {
    getAll,
    getById,
    create,
    update,
    remove
};


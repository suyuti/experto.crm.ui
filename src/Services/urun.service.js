import { http } from './axios';

function getAll() {
    return http.get("/api/urun")
}

function getById(id) {
    return http.get(`/api/urun/${id}`)
}

function create(urun) {
    return http.post('/api/urun', urun)
}

function update(urun) {
    return http.put(`/api/urun/${urun._id}`, urun)
}

function remove(urun) {
    return http.delete( `/api/urun/${urun._id}`)
}

 

export const urunService = {
    getAll,
    getById,
    create,
    update,
    remove, 
};


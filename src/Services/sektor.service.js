import { http } from './axios'

function getAll() {
    return http.get("/api/sektor")
}

function getById(id) {
    return http.get(`/api/sektor/${id}`)
}

function create(sektor) {
    return http.post('/api/sektor', sektor)
}

function update(sektor) {
    return http.put(`/api/sektor/${sektor._id}`, sektor)
}

function remove(sektor) {
    return http.delete( `/api/sektor/${sektor._id}`)
}

export const sektorService = {
    getAll,
    getById,
    create,
    update,
    remove
};


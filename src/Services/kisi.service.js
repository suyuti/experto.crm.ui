import { http } from './axios'

function getAll() {
    return http.get("/api/kisi")
}

function getById(id) {
    return http.get(`/api/kisi/${id}`)
}

function create(kisi) {
    return http.post('/api/kisi', kisi)
}

function update(kisi) {
    return http.put(`/api/kisi/${kisi._id}`, kisi)
}

function remove(kisi) {
    return http.delete( `/api/kisi/${kisi._id}`)
}
 

export const kisiService = {
    getAll,
    getById,
    create,
    update,
    remove, 
};


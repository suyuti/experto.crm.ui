import { http } from './axios';
 
function login(login) {
    return http.post('/api/login', login)
}

  
export const loginService = {
    login
}; 

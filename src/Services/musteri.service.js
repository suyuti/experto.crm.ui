import { http } from './axios'

function getAll() {
    return http.get("/api/musteri")
}

function getById(id) {
    return http.get(`/api/musteri/${id}`)
}

function create(musteri) {
    return http.post('/api/musteri', musteri)
}

function update(musteri) {
    return http.put(`/api/musteri/${musteri._id}`, musteri)
}

function remove(musteri) {
    return http.delete( `/api/musteri/${musteri._id}`)
}

function getTeklifler(id) {
    return http.get(`/api/musteri/${id}/teklifler`)
}
  
function getOnayBekleyenTeklifler(id) {
    return http.get(`/api/musteri/${id}/teklifler/onaybekleyen`)
}
function getOncekiDestekler(id) {
    return http.get(`/api/musteri/${id}/destek/history`)  
}
function newTeklifler(data) {
    return http.post(`/api/teklif`,data)
}

function getTeklifById(id) {
    return http.get(`/api/teklif/${id}`)
}

function updateTeklif(data) {
    return http.put(`/api/teklif`,data)
}
function updateTeklifOnayIste(id) {
    return http.put(`/api/teklif/${id}/onayiste`)
}
function updateTeklifOnayla(id) {
    return http.put(`/api/teklif/${id}/OnayVer`)
}
function updateTeklifSil(id) {
    return http.put(`/api/teklif/${id}/remove`)
}
   
function updateTeklifRedet(id) {
    return http.put(`/api/teklif/${id}/remove`)
}
export const musteriService = {
    getAll,
    getById,
    create,
    update,
    remove,
    getTeklifler,
    getOncekiDestekler,
    newTeklifler,
    getTeklifById,
    updateTeklif,
    updateTeklifOnayIste,
    getOnayBekleyenTeklifler,
    updateTeklifOnayla,
    updateTeklifSil,
    updateTeklifRedet
};


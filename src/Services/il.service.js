import { http } from './axios';

function getAll() {
    return http.get("/api/iller")
}

function getIlce(IlCode) {
    return http.get(`/api/iller/${IlCode}`)
}
 

export const ilService = {
    getAll, 
    getIlce
};


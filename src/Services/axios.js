
import axios from 'axios'
//import { API_URL } from '../config' 
var API_URL = 'http://localhost:4001'
// const API_URL = 'http://67.205.130.187:4001'

function parseError(messages) {
    // error
    if (messages) {
        if (messages instanceof Array) {
            return Promise.reject({ messages: messages })
        } else {
            return Promise.reject({ messages: [messages] })
        }
    } else {
        return Promise.reject({ messages: ['Bir hata oluştu'] })
    }
}

function parseBody(response) {
    //  if (response.status === 200 && response.data.status.code === 200) { // - if use custom status code
    if (response.status != 200) {
        console.error(response.data)
    }
    if (response.status === 200) {

        return response.data
    } else {
        return parseError(response.data.messages)
    }
}


let instance = axios.create({
    baseURL: API_URL
})



  async function refleshToken(config) { 
     //var a =  await axios.get(`${API_URL}/api/deneme`,{ 'headers': { 'Authorization': localStorage.getItem('token') }}); 
     
        //config.headers.jiwstmail= "jiwstmail";
        config.headers.Authorization=localStorage.getItem('token') 
        console.log(config);
        return    config   
}


// request header
instance.interceptors.request.use((config) => {
    // Do something before request is sent
    return refleshToken(config);

    // // api token
    // const apiToken = localStorage.getItem('token')
    // // config.headers = { 'Custom-Header-IF-Exist': apiToken }
    // //const apiToken = JSON.parse(localStorage.getItem('currentUser'));
    // // alert(apiToken.token)  
    // config['Content-Type'] = 'application/json'
    // config["headers"] = { 'Authorization': apiToken }

    // config.headers.test = 'added by interceptor';



    // config.headers.aaaa = aaa(config);


    // return config
}, error => {
    return Promise.reject(error)
})




// response parse
instance.interceptors.response.use((response) => {
    console.log(response.config.url + "  =>  ");
    console.log(response.data);
    return parseBody(response)
}, error => {
    console.warn('Error status', error)
    // return Promise.reject(error)
    if (error.response) {
        return parseError(error.response.data)
    } else {
        return Promise.reject(error)
    }
})






export const http = instance
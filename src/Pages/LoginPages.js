import React from 'react';
import { Row, Alert, Col, Image, Form, Tabs, Card, ButtonGroup, Button, Table, ProgressBar, Container, FormControl } from 'react-bootstrap'
import { loginService } from '../Services/login.service'


class LoginPages extends React.Component {
    state = {

    }

    handleChange = (value) => e => {
        this.setState({
            data: {
                ...this.state.data,
                [value]: e.target.value
            }
        }
        )
    }


    login = () => {
        // localStorage.setItem("currentUser","{adi:ismail}"); 

        loginService.login(this.state.data).then(response => {

            if (response.status !== 200) {
                this.setState({ messages: response.messages })
                return
            };
            localStorage.setItem("currentUser",JSON.stringify(response.data) );
            localStorage.setItem("token", response.data.Token);
            window.location.href = "/"
        })


    }
    render() {

        return (
            <Container fluid className="d-flex  justify-content-center align-items-center h-100 w-100">
                <Row >
                    <Col>
                        <Card style={{ padding: 30 }}>
                            <Form >
                                <Form.Group className="text-center">
                                    <h2>Giriş</h2>
                                </Form.Group>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Kullanıcı Adı</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Kullanıcı Adı"
                                        onChange={this.handleChange("KullaniciAdi")}
                                    />
                                    {/* <Form.Text className="text-muted">
                                        We'll never share your email with anyone else.
                                    </Form.Text> */}
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Şifre</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Şifre"
                                        onChange={this.handleChange("Password")}
                                    />
                                </Form.Group>
                                <Form.Group className="text-center">
                                    <Button onClick={this.login}>   Giriş</Button>
                                </Form.Group>

                                <Form.Group className="text-center">
                                    {this.state.messages&&<Alert variant="danger">
                                        {this.state.messages}
                                    </Alert>}
                                </Form.Group>


                            </Form>

                        </Card>
                    </Col>

                </Row>
            </Container>
        )
    };
}

export default LoginPages;
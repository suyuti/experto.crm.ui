import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
import { Form, Button, Card, Container, Row, Col } from 'react-bootstrap'
import { musteriService } from '../Services/musteri.service'

import MusteriForm from "../Components/MusteriForm"

import { withRouter } from 'react-router-dom';

var currentUser = JSON.parse(localStorage.getItem("currentUser"));



class TeklifPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            musteri: {},
            kisi: {},
            kisiler: [],
            teklifID: this.props.match.params.mid
        }
    }
    componentDidMount() {
        if (this.props.match.params.mid) {
            musteriService.getById(this.props.match.params.mid).then(response => {
                if (response.status !== 200) return;
                 this.setState({musteri:response.data[0]})
            })
        }  
    }


   
    onUpdate = (data) => {
        musteriService.update(this.state.musteri).then(response => {
            if (response.status !== 200) return; 
            this.props.history.goBack(); 
        }) 
    }
    onDelete = (data) => {
        // userService.remove(data).then(response => {
        //     if(response.status!==200) return;
        //     var newArray= this.state.data.filter((item)=>{
        //        return item._id!==data._id
        //     }) 
        //     this.state.data =newArray;
        //     this.setState({})
        // })
    }
    onHandleChangeModalData = (value) => e => {
        var tempData = this.state.musteri;
        var data;
        if (e.target && e.target.type == "number") {
            data = parseInt(e.target.value);
        } else if (e.target) {
            data = e.target.value
        }
        tempData[value] = e.target ? data : e
        this.setState({ musteri: tempData })
    }

    render() {
        return (<Card className="pb-5">
            <Container className="text-left">
                <Row>
                    <Col xs={12} className="p-3  " >
                        <Button variant="outline-secondary float-left"
                            onClick={() => {
                                this.props.history.goBack();
                            }}
                        >Go Back</Button>
                        <h1 className="text-center">Musteri Güncelleme</h1>
                    </Col>
                </Row>

                <hr />
                <Row>
                    <Col xs={12}>
                        <MusteriForm
                            onChangeHandler={this.onHandleChangeModalData}
                            tempModalData={this.state.musteri}
                        />
                    </Col>
                    <Col xs={12}>
                         
                            <button type="button" className="btn btn-outline-primary  btn-block" size="sm" onClick={this.onUpdate}>
                                Guncelle
                    </button>  

                    </Col>
                      {/* <Col>
                        {JSON.stringify(this.state.musteri)}
                    </Col>   */}
                </Row>
            </Container>


        </Card>
        )
    }
}


export default withRouter(TeklifPage)
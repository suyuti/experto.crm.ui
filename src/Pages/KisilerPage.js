import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
import { Form, Button, Card, Container, Row, Col } from 'react-bootstrap'
import { kisiService } from '../Services/kisi.service'

import KisiEklemeForm from "../Components/KisiEklemeForm"

import { withRouter } from 'react-router-dom';

var currentUser = JSON.parse(localStorage.getItem("currentUser"));



class TeklifPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            musteri: {},
            kisi: {},
            kisiler: [],
            teklifID: this.props.match.params.kid
        }
    }
    componentDidMount() {
        if (this.props.match.params.kid) {
            kisiService.getById(this.props.match.params.kid).then(response => {
                if (response.status !== 200) return;
                 this.setState({kisi:response.data[0]})
            })
        }  
    }


    onNew = () => {
        kisiService.create(this.state.kisi).then(response => {
            if (response.status !== 200) return;
            this.props.history.goBack();
        })
    }
    onUpdate = (data) => {
        kisiService.update(this.state.kisi).then(response => {
            if (response.status !== 200) return; 
            this.props.history.goBack(); 
        }) 
    }
    onDelete = (data) => {
        // userService.remove(data).then(response => {
        //     if(response.status!==200) return;
        //     var newArray= this.state.data.filter((item)=>{
        //        return item._id!==data._id
        //     }) 
        //     this.state.data =newArray;
        //     this.setState({})
        // })
    }
    onHandleChangeModalData = (value) => e => {
        var tempData = this.state.kisi;
        var data;
        if (e.target && e.target.type == "number") {
            data = parseInt(e.target.value);
        } else if (e.target) {
            data = e.target.value
        }
        tempData[value] = e.target ? data : e
        this.setState({ kisi: tempData })
    }

    render() {
        return (<Card className="pb-5">
            <Container className="text-left">
                <Row>
                    <Col xs={12} className="p-3  " >
                        <Button variant="outline-secondary float-left"
                            onClick={() => {
                                this.props.history.goBack();
                            }}
                        >Go Back</Button>
                        <h1 className="text-center">Kişi Ekleme</h1>
                    </Col>
                </Row>

                <hr />
                <Row>
                    <Col xs={12}>
                        <KisiEklemeForm
                            onChangeHandler={this.onHandleChangeModalData}
                            tempModalData={this.state.kisi}
                        />
                    </Col>
                    <Col xs={12}>
                        {this.state.kisi._id ?
                            <button type="button" className="btn btn-outline-primary  btn-block" size="sm" onClick={this.onUpdate}>
                                Guncelle
                    </button> :
                            <button type="button" className="btn btn-outline-primary  btn-block" size="sm" onClick={this.onNew}>
                                Ekle
                    </button>
                        }

                    </Col>
                    {/* <Col>
                        {JSON.stringify(this.state.kisi)}
                    </Col> */}
                </Row>
            </Container>


        </Card>
        )
    }
}


export default withRouter(TeklifPage)
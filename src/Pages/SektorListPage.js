import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
import { Form } from 'react-bootstrap'
import {sektorService}  from '../Services/sektor.service'


class SektorForm extends Component {
    render() {
        const {onChangeHandler} = this.props
        return(
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Adi</Form.Label>
                    <Form.Control type="text" placeholder="Adi" onChange={onChangeHandler("Adi")}/>
                    {/* <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text> */}
                </Form.Group> 
            </Form>        
        )
    }
}


export default class SektorListPage extends Component {
    state = {
        columns: [
            {text: 'id',                dataField: '_id',       hidden: true},
            {text: 'Sektor Adi',        dataField: 'Adi',      sort: true},
        ],
        data: []
    }

    componentDidMount() {
        sektorService.getAll().then(response => {
            this.setState({data: response.data})
        })
        .catch(e => console.log(e))
    }
    onNew=(data)=>{ 
        sektorService.create(data).then(response=>{  
            this.setState({data:[ ...this.state.data   ,response.data]}) 
            
        }) 
        return true
    }
    onUpdate=()=>{

    }
    onDelete=(data)=>{

        sektorService.remove(data).then(response=>{  
            if(response.status!==200) return;
            var newData= this.state.data .filter((item)=>{
                return item._id!==data._id
            })
            this.setState({data:[ ...newData]}) 
            
        }) 

    }
    render() {
        const {data, columns} = this.state
        return (
            <ListSearchPage 
                title = "Sektorler" 
                subtitle = "" 
                columns={columns}
                data={data} 
                ModelForm={SektorForm} 
                striped 
                condensed 
                hover   
                linkurl='/musteri'   
                onNew = {this.onNew}
                onUpdate={this.onUpdate}
                onDelete={this.onDelete}
                />
        )
    }
}

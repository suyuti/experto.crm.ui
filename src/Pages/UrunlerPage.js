import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
// import { Form, Button } from 'react-bootstrap'
import {urunService}  from '../Services/urun.service'
import UrunEklemeForm from "../Components/UrunEklemeForm"





export default class UrunlerPage extends Component {
    state = {
        columns: [
            {text: 'id',                dataField: '_id',            hidden: true},
            {text: 'Ürün Adı',         dataField: 'Adi',    },
            {text: 'Aktif Mi',    dataField: 'AktifMi'},
            {text: 'Ön Ödeme Tutarı',    dataField: 'OnOdemeTutari'},
            {text: 'Rapor Başı Ödeme Tutarı',  dataField: 'RaporBasiOdemeTutari'   },
            {text: 'Yüzde Tutarı',           dataField: 'YuzdeTutari' },
            {text: 'Sabit Tutar',           dataField: 'SabitTutar' },
            {text: 'Başarı Tutarı',           dataField: 'BasariTutari' },
            {text: 'KDV Oranı',           dataField: 'KDVOrani' },
        ],
        data: [],
        modalOpen:false
    }
    componentDidMount() {
        urunService.getAll().then(response => {
            this.setState({data: response.data})
        })
    }


    onNew=(data)=>{
        urunService.create(data).then(response => {
            if(response.status!==200) return;   
            //this.state.data.push(data);
            var new_Data = this.state.data.concat(response.data) ;
            this.setState({data:new_Data});
        })
        return true
    }
    onUpdate=(data)=>{
        urunService.update(data).then(response => {
            if(response.status!==200) return;
            console.log(data)
            var updated= this.state.data.map(item => {
                if (item._id == data._id) {

                  return data;
                } else {
                  return item;
                } 
            })  
            this.setState({data:updated }) ;
            this.setState({modalOpen:false }) ;
            console.log("modalOpen: false");
            
        })


    }
    onDelete=(data)=>{ 
        urunService.remove(data).then(response => {
            if(response.status!==200) return;
            var newArray= this.state.data.filter((item)=>{
               return item._id!==data._id
            }) 
            this.state.data =newArray;
            this.setState({})
        })
    }

    render() {
        const {data, columns} = this.state
        return (<> 
            <ListSearchPage 
                title = "Ürünler" 
                subtitle = "Ürün Ekleme Formu" 
                columns={columns}
                linkurl='/urun' 
                data={data} 
                ModelForm={UrunEklemeForm} 
                striped 
                condensed 
                hover
                onNew = {this.onNew}
                onUpdate={this.onUpdate}
                onDelete={this.onDelete} 
                modalOpen={this.state.modalOpen}
                />
                </>
        )
    }
}

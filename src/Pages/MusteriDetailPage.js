import React, { Component, useContext } from 'react'
import { Row, Col, Image, Tab, Tabs, Card, ButtonGroup, Button, Table, ProgressBar, Container, Modal, Form } from 'react-bootstrap'
import styled from 'styled-components'
import { musteriService } from '../Services/musteri.service'
import { kisiService } from '../Services/kisi.service'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash, faSpinner, faMailBulk, faPlus, faGlobe, faPhone, faMapMarked } from '@fortawesome/free-solid-svg-icons';
import { SnackbarConsumer } from "../Context/SnackbarContext"
import SnackbarContext from "../Context/SnackbarContext"
import { Link } from 'react-router-dom';
import { withRouter } from "react-router-dom";


var MusteriBilgi = withRouter(class MusteriBilgi extends Component {
    constructor(props) {
        super(props)
        this.state = {
            musteri: {}
        }
    }

    componentDidMount() {
        musteriService.getById(this.props.musteriID).then((response) => {
            this.setState({ musteri: response.data[0] });
            console.log("componentDidMount >  musteriService.getById");
        })
    }


    render() {
        if (JSON.stringify(this.state.musteri) === '{}') {
            return (<div id="MusteriBilgi">
                <Card id="loader" >
                    <FontAwesomeIcon className="loader-icon" icon={faSpinner} spin={true} />
                </Card>
            </div>)
        }


        return (
            <div id="MusteriBilgi">
                <div className="edit">
                    <ButtonGroup size="sm">
                        <Button
                            variant="outline-secondary"
                            onClick={() => {
                                this.props.history.push(`/musteri/${this.props.musteriID}/update`);
                            }}
                        ><FontAwesomeIcon icon={faEdit} /></Button>
                        {/* <Button variant="outline-danger"><FontAwesomeIcon icon={ faTrash}  /></Button>  */}
                    </ButtonGroup>

                </div>
                <Card  >
                    <Card.Body className="text-center">
                        <Image className="avatar" src={this.state.musteri.logo ? this.state.musteri.logo : "/musteri.png"} width={200} roundedCircle thumbnail />
                        <Card.Title className="mt-2" >
                            {this.state.musteri.FirmaUnvani}<br />
                            <p className="badge badge-danger d-block">Potansiyel</p>
                        </Card.Title>
                        <hr />
                        <Row className="text-left">
                            <Col xs={12}> <small> <strong> Sektor/OSB</strong> </small> </Col>
                            <Col>
                                {this.state.musteri.Sektor}
                            </Col>
                            /
                            <Col>
                                {this.state.musteri.OSB}
                            </Col>
                        </Row>
                        <Row className="text-left">
                            <Col xs={12}> <small> <strong>Adres</strong> </small> </Col>
                            <Col>
                                {this.state.musteri.Adres}
                                <div className="text-right"> {this.state.musteri.ilce} / {this.state.musteri.il} </div>
                            </Col>
                        </Row>
                        <hr />
                        <Row className="text-left">
                            <Col >
                                <strong>  <FontAwesomeIcon icon={faPhone} />   <small><a href={`tel:${this.state.musteri.Telefon}`}>{this.state.musteri.Telefon}</a></small></strong>
                                <br />

                                <strong>  <FontAwesomeIcon icon={faGlobe} />   <small><a href={`http://${this.state.musteri.Web}`}>{this.state.musteri.Web}</a></small></strong>
                                <br />

                                <strong>  <FontAwesomeIcon icon={faMailBulk} />   <small><a href={`mailto:${this.state.musteri.Mail}`}>{this.state.musteri.Mail}</a></small></strong>
                                <br />

                            </Col>
                        </Row>
                        <hr />
                        <Row className="text-center">
                            <Col xs={4}>
                                <strong><small>Çalışan</small></strong>
                                <p> {this.state.musteri.CalisanSayisi ? this.state.musteri.CalisanSayisi : 0} </p>
                            </Col>
                            <Col xs={4}>
                                <strong><small>Teknik</small></strong>
                                <p> {this.state.musteri.TeknikPersonelSayisi ? this.state.musteri.TeknikPersonelSayisi : 0} </p>
                            </Col>
                            <Col xs={4}>
                                <strong><small>Muhendis</small></strong>
                                <p> {this.state.musteri.MuhendisSayisi ? this.state.musteri.MuhendisSayisi : 0} </p>
                            </Col>
                        </Row>


                    </Card.Body>
                </Card>
            </div>
        )
    }
})

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------


class GorusmeNotlari extends Component {
    // static contextType = SnackbarContext;

    componentDidMount() {
        console.log(this.context)
    }
    render() {
        return (
            <Card id="GorusmeNotlari">
                gorusme notlari   {this.context.type}
            </Card>
        )
    }
}
GorusmeNotlari.contextType = SnackbarContext;
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

var Teklifler = withRouter(class Teklifler extends Component {

    constructor(props) {
        super(props)
        this.state = {
            teklifler: [],
            tablo: null,
            verilen: 0,
            bekleyen: 0,
            basarili: 0,
            basarisiz: 0,
            model: false,
            TeklifDurumu: [
                { id: 0, name: "Yeni Teklif Oluşturuldu" },
                { id: 1, name: "Onay Bekleniyor" },
                { id: 10, name: "Onay verildi" },
                { id: 101, name: "İlk toplantı yapıldı" },
                { id: 102, name: "Teklif Verildi" },
                { id: 103, name: "Ön teknik toplantı yapıldı" },
                { id: 104, name: "Sözleşme Gönderildi" },
                { id: 105, name: "Sözleşme yapıldı " },
                { id: 400, name: "Kaybedildi" },
                { id: 401, name: "Teklif Silindi" },
                { id: 402, name: "Teklif Red Edildi" },
            ],
            modelData: {},
            tempTeklifDurumu: null
        }
    }

    componentDidMount() {
        var responseData = []
        musteriService.getTeklifler(this.props.musteriID).then((response) => {
            responseData = response.data;
            var verilenCount = responseData.filter((item) => {
                return 102 <= item.TeklifDurumu && item.TeklifDurumu < 105
            });
            var bekleyenCount = responseData.filter((item) => {
                return item.TeklifDurumu < 102
            });
            var basariliCount = responseData.filter((item) => {
                return item.TeklifDurumu == 105
            });
            var basarisizCount = responseData.filter((item) => {
                return item.TeklifDurumu == 400
            });
            var tablo = responseData.filter((item) => {
                return item.TeklifDurumu !== 400 && item.TeklifDurumu !== 105
            });
            tablo = tablo.map((item) => {
                if (item.TeklifDurumu)
                    switch (item.TeklifDurumu) {
                        case 101:
                            item["TeklifDurumuYuzdesi"] = 10
                            break;
                        case 102:
                            item["TeklifDurumuYuzdesi"] = 40
                            break;
                        case 103:
                            item["TeklifDurumuYuzdesi"] = 60
                            break;
                        case 104:
                            item["TeklifDurumuYuzdesi"] = 80
                            break;
                    }
                return item
            });
            this.setState({
                teklifler: responseData,
                tablo: tablo,
                verilen: verilenCount.length,
                bekleyen: bekleyenCount.length,
                basarili: basariliCount.length,
                basarisiz: basarisizCount.length
            });

            console.log(this.state.tablo)

            //   this.setState({ teklifler: response.data}); 
        });
    }
    handleModelClose = () => {
        this.setState({ model: false })
    }
    handleModelSave = () => {
        musteriService.updateTeklif(this.state.modelData).then((response) => {
            if (response.status != 200) return;
            var newTeklifler = this.state.tablo.map((item) => {
                if (item._id == this.state.modelData._id) {
                    return this.state.modelData
                }
                return item;
            })

            this.setState({ tablo: newTeklifler })
            this.setState({ model: false })
        });

    }
    handleModelOpen = (item) => {
        this.setState({ model: true, modelData: item, tempTeklifDurumu: item.TeklifDurumu })
    }
    handleChange = value => e => {
        console.log(e.target.value)
        this.setState({
            modelData: {
                ...this.state.modelData,
                [value]: e.target.value
            }
        })

    }

    getTeklifDurumu = (id) => {
        var find = this.state.TeklifDurumu.filter((item) => {
            return item.id == id
        })
        return find[0].name
    }
    //tempTeklifDurumu    
    getTeklifDurumuCheckBox = (item) => {
        if (this.state.modelData.TeklifDurumu == 99) {
            return (<Form.Check
                item="checkbox"
                checked
                disabled
                id={` ${item.id}`}
                label={`${item.name}`}
            />)
        }
        else if (this.state.modelData.TeklifDurumu >= item.id) {
            if (this.state.tempTeklifDurumu >= item.id) {
                return (
                    <Form.Check
                        onChange={this.handleChange("TeklifDurumu")}
                        item="checkbox"
                        checked
                        disabled
                        id={` ${item.id}`}
                        label={`${item.name}`}
                    />
                )
            } else {
                return (
                    <Form.Check
                        onChange={this.handleChange("TeklifDurumu")}
                        item="checkbox"
                        checked
                        id={` ${item.id}`}
                        label={`${item.name}`}
                    />
                )
            }
        } else {
            return (
                <Form.Check
                    onChange={this.handleChange("TeklifDurumu")}
                    value={item.id}
                    item="checkbox"
                    id={`${item.id}`}
                    label={`${item.name}`}
                />
            )
        }

    }

    handleOnayIste = (data) => {
        musteriService.updateTeklifOnayIste(data._id).then((response) => {
            if (response.status != 200) return;
            var newTeklifler = this.state.tablo.map((item) => {
                if (item._id == data._id) {
                    item.TeklifDurumu = 1
                    return item
                }
                return item;
            })

            this.setState({ tablo: newTeklifler })
            this.setState({ model: false })
        });

    }
    updateTeklifSil = (data) => {
        musteriService.updateTeklifSil(data._id).then((response) => {
            if (response.status != 200) return;
            var newTeklifler = this.state.tablo.filter((item) => {
                return item._id !== data._id
            })

            this.setState({ tablo: newTeklifler })
            this.setState({ model: false })
        });
    }


    render() {
        if (this.state.tablo === null) {
            return (
                <div id="Teklifler">
                    <Card id="loader" >
                        <FontAwesomeIcon className="loader-icon" icon={faSpinner} spin={true} />
                    </Card>
                </div>)
        }
        return (<>
            <Card id="Teklifler">
                <Row  >
                    <Col >
                        <h3 className="title">Teklifler


                            <Button variant="outline-success float-right"
                                onClick={() => {
                                    this.props.history.push(`/musteri/${this.props.musteriID}/teklif`);
                                }} >
                                <FontAwesomeIcon icon={faPlus} />
                            </Button>
                        </h3>
                    </Col>
                </Row>
                <hr />
                {this.state.tablo.length !== 0 && <Row className="justify-content-md-center">
                    <Col xs={10}>
                        <Table responsive>
                            <thead>
                                <tr>
                                    {/* <th>#</th> */}
                                    <th>#</th>
                                    <th>Urun Adı</th>
                                    <th>OnOdemeTutari</th>
                                    <th>VerilisTarihi</th>
                                    <th>Sorumlu</th>
                                    <th>Teklif Suresi</th>
                                    <th>Durumu</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.tablo.map((item, index) => {
                                    return (<tr>
                                        {/* <td>{index}</td> */}
                                        <td>

                                            {(item.TeklifDurumu == 0) && <Link to={`/musteri/${this.props.musteriID}/teklif/${item._id}`}>
                                                {/* {item.Urun.Adi} */}
                                                <FontAwesomeIcon icon={faEdit} />
                                            </Link>
                                            }
                                        </td>
                                        <td>
                                            <Button onClick={() => this.handleModelOpen(item)} size="sm">
                                                {item.Urun.Adi}
                                            </Button>
                                        </td>
                                        <td>{item.OnOdemeTutari}</td>
                                        <td>{item.VerilisTarihi.gunAyYil()}</td>
                                        <td>{item.IlgiliKisi && item.IlgiliKisi.Adi}</td>
                                        <td> </td>
                                        <td>
                                            {item.TeklifDurumu == 0 ?
                                                <Button onClick={() => this.handleOnayIste(item)} size="sm">
                                                    Onay İste
                                            </Button> :
                                                this.getTeklifDurumu(item.TeklifDurumu)
                                            }

                                            {(item.TeklifDurumu == 0 || item.TeklifDurumu == 1) &&
                                                <Button variant="danger" size="sm" onClick={() => this.updateTeklifSil(item)}>
                                                    Sil
                                            </Button>
                                            }

                                            {/* <ProgressBar now={item.TeklifDurumuYuzdesi} /> */}
                                        </td>
                                    </tr>)

                                })}


                            </tbody>
                        </Table>
                    </Col>
                </Row>}
                <hr className={`${this.state.tablo.length == 0 && " d-none "}`} />
                <Row className={`text-center ${this.state.tablo.length == 0 && " d-none "}`}>
                    <Col >
                        <strong>Verilen</strong><br /><small>{this.state.verilen}</small>
                    </Col>
                    <Col >
                        <strong>Bekleyen</strong><br /><small>{this.state.bekleyen}</small>
                    </Col>
                    <Col >
                        <strong>Basarılı</strong><br /><small>{this.state.basarili}</small>
                    </Col>

                </Row>

                <Row className={`d-none  text-center ${this.state.tablo.length == 0 && " d-flex "}`}>
                    <Col >
                        <h4>Veri Yok</h4>
                    </Col>
                </Row>


            </Card>
            <Modal show={this.state.model} onHide={this.handleModelClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Teklif Durumu</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Form>
                        {this.state.TeklifDurumu.map(item => (
                            <div key={`default-${item}`} className="mb-3">
                                {this.getTeklifDurumuCheckBox(item)}
                            </div>
                        ))}
                    </Form>


                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleModelClose}>
                        İptal
                    </Button>
                    <Button variant="success" onClick={this.handleModelSave}>
                        Degişikligi Kaydet
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
        )
    }
})
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

var OnayBekleyenTeklifler = withRouter(class OnayBekleyenTeklifler extends Component {

    constructor(props) {
        super(props)
        this.state = {
            teklifler: [],
            tablo: null,
            verilen: 0,
            bekleyen: 0,
            basarili: 0,
            basarisiz: 0,
            model: false,
            TeklifDurumu: [
                { id: 0, name: "Yeni Teklif Oluşturuldu" },
                { id: 1, name: "Onay Bekleniyor" },
                { id: 10, name: "Onay verildi" },
                { id: 101, name: "İlk toplantı yapıldı" },
                { id: 102, name: "Teklif Verildi" },
                { id: 103, name: "Ön teknik toplantı yapıldı" },
                { id: 104, name: "Sözleşme Gönderildi" },
                { id: 105, name: "Sözleşme yapıldı " },
                { id: 400, name: "Kaybedildi" },
                { id: 401, name: "Teklif Silindi" },
                { id: 402, name: "Teklif Red Edildi" },
            ],
            modelData: {},
            tempTeklifDurumu: null
        }
    }

    componentDidMount() {
        var responseData = []
        musteriService.getOnayBekleyenTeklifler(this.props.musteriID).then((response) => {
            responseData = response.data;
            var verilenCount = responseData.filter((item) => {
                return 102 <= item.TeklifDurumu && item.TeklifDurumu < 105
            });

            var tablo = responseData.filter((item) => {
                return item.TeklifDurumu !== 400 && item.TeklifDurumu !== 105
            });
            tablo = tablo.map((item) => {
                if (item.TeklifDurumu)
                    switch (item.TeklifDurumu) {
                        case 101:
                            item["TeklifDurumuYuzdesi"] = 10
                            break;
                        case 102:
                            item["TeklifDurumuYuzdesi"] = 40
                            break;
                        case 103:
                            item["TeklifDurumuYuzdesi"] = 60
                            break;
                        case 104:
                            item["TeklifDurumuYuzdesi"] = 80
                            break;
                    }
                return item
            });
            this.setState({
                teklifler: responseData,
                tablo: tablo,
                verilen: verilenCount.length,
            });

            console.log(this.state.tablo)

            //   this.setState({ teklifler: response.data}); 
        });
    }
    handleModelClose = () => {
        this.setState({ model: false })
    }
    handleModelSave = () => {
        musteriService.updateTeklif(this.state.modelData).then((response) => {
            if (response.status != 200) return;
            var newTeklifler = this.state.tablo.map((item) => {
                if (item._id == this.state.modelData._id) {
                    return this.state.modelData
                }
                return item;
            })

            this.setState({ tablo: newTeklifler })
            this.setState({ model: false })
        });

    }
    handleModelOpen = (item) => {
        this.setState({ model: true, modelData: item, tempTeklifDurumu: item.TeklifDurumu })
    }
    handleChange = value => e => {
        console.log(e.target.value)
        this.setState({
            modelData: {
                ...this.state.modelData,
                [value]: e.target.value
            }
        })

    }

    getTeklifDurumu = (id) => {
        var find = this.state.TeklifDurumu.filter((item) => {
            return item.id == id
        })
        return find[0].name
    }
    //tempTeklifDurumu    
    getTeklifDurumuCheckBox = (item) => {
        if (this.state.modelData.TeklifDurumu == 99) {
            return (<Form.Check
                item="checkbox"
                checked
                disabled
                id={` ${item.id}`}
                label={`${item.name}`}
            />)
        }
        else if (this.state.modelData.TeklifDurumu >= item.id) {
            if (this.state.tempTeklifDurumu >= item.id) {
                return (
                    <Form.Check
                        onChange={this.handleChange("TeklifDurumu")}
                        item="checkbox"
                        checked
                        disabled
                        id={` ${item.id}`}
                        label={`${item.name}`}
                    />
                )
            } else {
                return (
                    <Form.Check
                        onChange={this.handleChange("TeklifDurumu")}
                        item="checkbox"
                        checked
                        id={` ${item.id}`}
                        label={`${item.name}`}
                    />
                )
            }
        } else {
            return (
                <Form.Check
                    onChange={this.handleChange("TeklifDurumu")}
                    value={item.id}
                    item="checkbox"
                    id={`${item.id}`}
                    label={`${item.name}`}
                />
            )
        }

    }

    handleOnayla = (data) => {
        musteriService.updateTeklifOnayla(data._id).then((response) => {
            if (response.status != 200) return;
            var newTeklifler = this.state.tablo.map((item) => {
                if (item._id == data._id) {
                    item.TeklifDurumu = 10
                    return item
                }
                return item;
            })

            this.setState({ tablo: newTeklifler })
            this.setState({ model: false })
        });

    }
    updateTeklifRedet = (data) => {
        musteriService.updateTeklifRedet(data._id).then((response) => {
            if (response.status != 200) return;
            var newTeklifler = this.state.tablo.filter((item) => {
                return item._id !== data._id
            })

            this.setState({ tablo: newTeklifler })
            this.setState({ model: false })
        });
    }


    render() {
        if (this.state.tablo === null) {
            return (
                <div id="Teklifler">
                    <Card id="loader" >
                        <FontAwesomeIcon className="loader-icon" icon={faSpinner} spin={true} />
                    </Card>
                </div>)
        }
        return (<>
            <Card id="Teklifler">
                <Row  >
                    <Col >
                        <h3 className="title"> Onay Bekleyen Teklifler


                            <Button variant="outline-success float-right"
                                onClick={() => {
                                    this.props.history.push(`/musteri/${this.props.musteriID}/teklif`);
                                }} >
                                <FontAwesomeIcon icon={faPlus} />
                            </Button>
                        </h3>
                    </Col>
                </Row>
                <hr />
                {this.state.tablo.length !== 0 && <Row className="justify-content-md-center">
                    <Col xs={10}>
                        <Table responsive>
                            <thead>
                                <tr>
                                    {/* <th>#</th> */}
                                    <th>#</th>
                                    <th>Urun Adı</th>
                                    <th>OnOdemeTutari</th>
                                    <th>VerilisTarihi</th>
                                    <th>Sorumlu</th>
                                    <th>Teklif Suresi</th>
                                    <th>Durumu</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.tablo.map((item, index) => {
                                    return (<tr>
                                        {/* <td>{index}</td> */}
                                        <td>

                                            {(item.TeklifDurumu == 0) && <Link to={`/musteri/${this.props.musteriID}/teklif/${item._id}`}>
                                                {/* {item.Urun.Adi} */}
                                                <FontAwesomeIcon icon={faEdit} />
                                            </Link>
                                            }
                                        </td>
                                        <td>
                                            <Button onClick={() => this.handleModelOpen(item)} size="sm">
                                                {item.Urun.Adi}
                                            </Button>
                                        </td>
                                        <td>{item.OnOdemeTutari}</td>
                                        <td>{item.VerilisTarihi.gunAyYil()}</td>
                                        <td>{item.IlgiliKisi && item.IlgiliKisi.Adi}</td>
                                        <td> </td>
                                        <td>
                                            {item.TeklifDurumu == 1 ?
                                              <>  <Button onClick={() => this.handleOnayla(item)} size="sm"> Onayla </Button>
                                                <Button variant="danger" size="sm" onClick={() => this.updateTeklifRedet(item)}> Reddet </Button>  
                                              </> :
                                                this.getTeklifDurumu(item.TeklifDurumu)
                                            }


                                            {/* <ProgressBar now={item.TeklifDurumuYuzdesi} /> */}
                                        </td>
                                    </tr>)

                                })}


                            </tbody>
                        </Table>
                    </Col>
                </Row>}
                {/* <hr className={`${this.state.tablo.length == 0 && " d-none "}`} />
                <Row className={`text-center ${this.state.tablo.length == 0 && " d-none "}`}>
                    <Col >
                        <strong>Verilen</strong><br /><small>{this.state.verilen}</small>
                    </Col>
                    <Col >
                        <strong>Bekleyen</strong><br /><small>{this.state.bekleyen}</small>
                    </Col>
                    <Col >
                        <strong>Basarılı</strong><br /><small>{this.state.basarili}</small>
                    </Col>

                </Row> */}

                <Row className={`d-none  text-center ${this.state.tablo.length == 0 && " d-flex "}`}>
                    <Col >
                        <h4>Veri Yok</h4>
                    </Col>
                </Row>


            </Card>
            <Modal show={this.state.model} onHide={this.handleModelClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Teklif Durumu</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Form>
                        {this.state.TeklifDurumu.map(item => (
                            <div key={`default-${item}`} className="mb-3">
                                {this.getTeklifDurumuCheckBox(item)}
                            </div>
                        ))}
                    </Form>


                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleModelClose}>
                        İptal
                    </Button>
                    <Button variant="success" onClick={this.handleModelSave}>
                        Degişikligi Kaydet
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
        )
    }
})

//---------------------------------------------------------------------------------------------

var OncekiDestekler = withRouter(class OncekiDestekler extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tablo: null,
        }
    }

    componentDidMount() {
        musteriService.getOncekiDestekler(this.props.musteriID).then((response) => {
            this.setState({
                tablo: response.data
            });
        });
    }

    _getDurum(durum) {
        var text = ""
        switch (durum) {
            case 1:
                text = "Devam Ediyor"
                break;
            case 99:
                text = "Başarısız"
                break;
            case 200:
                text = "Başarılı"
                break;
            default:
                text = "Bilinmiyor"
                break;
        }
        return text
    }
    _getColor(status) {
        var text = ""
        switch (status) {
            case 1://Devam Ediyor
                text = "badge-info"
                break;
            case 99://Başarısız
                text = "badge-danger"
                break;
            case 200://Başarılı
                text = "badge-success"
                break;
            default://Bilinmiyor
                text = "badge-default"
                break;
        }
        return text
    }

    render() {
        if (this.state.tablo == null) {
            return (
                <div id="OncekiDestekler">
                    <Card id="loader" >
                        <FontAwesomeIcon className="loader-icon" icon={faSpinner} spin={true} />
                    </Card>
                </div>)
        }
        return (
            <Card id="OncekiDestekler">
                <Row  >
                    <Col >
                        <h3 className="title">Onceki Destekler

                            <Button variant="outline-success float-right"
                                onClick={() => {
                                    this.props.history.push(`/musteri/${this.props.musteriID}/oncekidestekler`);
                                }} >
                                <FontAwesomeIcon icon={faPlus} />
                            </Button>
                        </h3>
                    </Col>
                </Row>
                <hr />
                <Row className={` justify-content-md-center ${this.state.tablo.length == 0 && " d-none "}`} >
                    <Col xs={10}>
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th>Ürün Adı</th>
                                    <th>Tarih</th>
                                    <th>Durumu</th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.state.tablo.map((item, index) => {
                                    return (<tr>
                                        <td>
                                            <Link to={`/musteri/${this.props.musteriID}/oncekidestekler/${item._id}`}>
                                                {item.Adi}
                                            </Link>

                                        </td>
                                        <td>{item.Tarih.gunAyYil()}</td>
                                        <td><p className={`badge ${this._getColor(item.Durum)}`}>{this._getDurum(item.Durum)}</p> </td>
                                    </tr>)

                                })}


                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row className={`d-none  text-center ${this.state.tablo.length == 0 && " d-flex "}`}>
                    <Col >
                        <h4>Veri Yok</h4>
                    </Col>
                </Row>


            </Card>

        )
    }
})

//---------------------------------------------------------------------------------------------
var Kisiler = withRouter(class Kisiler extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tablo: [1, 2, 3, 5],
            kisiler: []
        }
    }

    componentDidMount() {
        kisiService.getAll().then((response) => {
            this.setState({
                kisiler: response.data
            });
        });
    }
    render() {
        return (
            <Card id="Kisiler">
                <Row  >
                    <Col >
                        <h3 className="title">Kisiler
                            <Button variant="outline-success float-right"
                                onClick={() => {
                                    this.props.history.push(`/musteri/${this.props.musteriID}/kisiler`);
                                }} >
                                <FontAwesomeIcon icon={faPlus} />
                            </Button>
                        </h3>
                    </Col>
                </Row>
                <hr />


                <Row>
                    <Col md={12} lg={4}  >
                        <Col xs={12}>Yönetim</Col>
                        <Col xs={12}>
                            {this.state.kisiler.filter(item => { return item.Type == "1" }).map((item) => {
                                return (
                                    <Row className="mt-1 border border-primary rounded-pill">
                                        <Col xs={3} className="d-flex align-items-center">
                                            <img src="/musteri.png " class="rounded w-100 " alt="Cinque Terre" />
                                        </Col>
                                        <Col xs={9} >
                                            <Link to={`/musteri/${this.props.musteriID}/kisiler/${item._id}`}>
                                                {`${item.Adi ? item.Adi : ""} ${item.Soyadi ? item.Soyadi : ""}`}
                                            </Link>
                                            <br />
                                            <small>{item.Unvani}</small>
                                            <br />
                                            <div className="d-flex">
                                                <div className="flex-grow-1">{item.CepTel} </div>
                                            </div>

                                        </Col>
                                    </Row>)
                            })}
                        </Col>
                    </Col>
                    <Col md={12} lg={4}  >
                        <Col xs={12}>Mali</Col>
                        <Col xs={12}>
                            {this.state.kisiler.filter(item => { return item.Type == "2" }).map((item) => {
                                return (
                                    <Row className="mt-1">
                                        <Col xs={3} className="d-flex align-items-center">
                                            <img src="/musteri.png " class="rounded w-100 " alt="Cinque Terre" />
                                        </Col>
                                        <Col xs={9} >
                                            <Link to={`/musteri/${this.props.musteriID}/kisiler/${item._id}`}>
                                                {`${item.Adi ? item.Adi : ""} ${item.Soyadi ? item.Soyadi : ""}`}
                                            </Link>
                                            <br />
                                            <small>{item.Unvani}</small>
                                            <br />
                                            <div className="d-flex">
                                                <div className="flex-grow-1">{item.CepTel} </div>
                                            </div>

                                        </Col>
                                    </Row>)
                            })}
                        </Col>
                    </Col>
                    <Col md={12} lg={4}  >
                        <Col xs={12}>Teknik</Col>
                        <Col xs={12}>
                            {this.state.kisiler.filter(item => { return item.Type == "3" }).map((item) => {
                                return (
                                    <Row className="mt-1">
                                        <Col xs={3} className="d-flex align-items-center">
                                            <img src="/musteri.png " class="rounded w-100 " alt="Cinque Terre" />
                                        </Col>
                                        <Col xs={9} >
                                            <Link to={`/musteri/${this.props.musteriID}/kisiler/${item._id}`}>
                                                {`${item.Adi ? item.Adi : ""} ${item.Soyadi ? item.Soyadi : ""}`}
                                            </Link>
                                            <br />
                                            <small>{item.Unvani}</small>
                                            <br />
                                            <div className="d-flex">
                                                <div className="flex-grow-1">{item.CepTel} </div>
                                            </div>

                                        </Col>
                                    </Row>)
                            })}
                        </Col>
                    </Col>
                </Row>
            </Card>
        )
    }
})

//---------------------------------------------------------------------------------------------

function danismanTable(danismanlar) {
    if (danismanlar == undefined || danismanlar.length == 0) {
        return (
            <Row className={`d-none  text-center  d-flex `}>
                <Col >
                    <h4>Danışmanı yok</h4>
                </Col>
            </Row>
        )
    }
    return (
        <table class="table table-sm">
            <thead>
                <tr>
                    <th scope="col">Danisman Adi</th>
                    <th scope="col">Telefonu</th>
                    <th scope="col">Adresi</th>
                </tr>
            </thead>
            <tbody>
                {danismanlar.map(d => {
                    return (
                        <tr>
                            <td>{d.adi}</td>
                            <td>{d.telefon}</td>
                            <td>{d.adres}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>)
}

class Danisman extends Component {
    render() {
        return (<></>)
        return (
            <Card id="Kisiler">
                <Row  >
                    <Col >
                        <h3 className="title">Kişiler  <Button variant="outline-success float-right"><FontAwesomeIcon icon={faPlus} /></Button></h3>
                    </Col>
                </Row>
                <hr />


                <Row>
                    <Col md={12} lg={4}  >
                        <h3>Yönetim</h3>
                        {this.state.yonetim.map(() => {
                            return (
                                <Row className="mt-1">
                                    <Col xs={3} className="d-flex align-items-center">
                                        <img src="/musteri.png " class="rounded w-100 " alt="Cinque Terre" />
                                    </Col>
                                    <Col xs={9} >
                                        isnim
                                        <br />
                                        <small>sadasd</small>
                                        <br />
                                        <div className="d-flex">
                                            <div className="flex-grow-1">sadasd </div>
                                            <div className="flex-grow-1">sadasd </div>
                                            <div className="flex-grow-1">sadasd </div>
                                        </div>

                                    </Col>
                                </Row>)
                        })}
                    </Col>
                    <Col md={12} lg={4} className="kisi-list">
                        <h3>Teknik</h3>
                        {this.state.teknik.map(t => {
                            return (
                                <Row className="mt-1">
                                    <Col xs={3} className="d-flex align-items-center">
                                        <img src="/musteri.png " class="rounded w-100 " alt="Cinque Terre" />
                                    </Col>
                                    <Col xs={9} >
                                        isnim
                                        <br />
                                        <small>sadasd</small>
                                        <br />
                                        <div className="d-flex">
                                            <div className="flex-grow-1">sadasd </div>
                                            <div className="flex-grow-1">sadasd </div>
                                            <div className="flex-grow-1">sadasd </div>
                                        </div>

                                    </Col>
                                </Row>)
                        })}
                    </Col>
                </Row>
            </Card>
        )
    }
}



function MusteriBar(musteri) {
    return (
        <table className="musteri-bar">
            <tbody>
                <tr>
                    <td>
                        <Image className="avatar" src="/musteri.png" roundedCircle />
                    </td>
                    <td>
                        <h2 className="musteri-adi">{musteri.adi} <small> / {musteri.sektor.adi}, {musteri.osb}</small></h2>
                    </td>
                </tr>
            </tbody>
        </table>
    )
}
function MusteriInfo(musteri) {
    return (
        <table className="musteri-info-table">
            <tbody>
                <tr>
                    <td className="table-label-col">
                        <h2 className="musteri-info-label">Kayit sahibi</h2>
                    </td>
                    <td className="table-info-col">
                        ...
                    </td>
                </tr>
                <tr>
                    <td className="table-label-col">
                        <h2 className="musteri-info-label">Sektor</h2>
                    </td>
                    <td className="table-info-col">
                        {musteri.sektor.adi || ''}
                    </td>
                </tr>
                <tr>
                    <td className="table-label-col">
                        <h2 className="musteri-info-label">Calisan sayisi</h2>
                    </td>
                    <td className="table-info-col">
                        {musteri.calisanSayisi || ''}
                    </td>
                </tr>
                <tr>
                    <td className="table-label-col">
                        <h2 className="musteri-info-label">Yillik Gelir</h2>
                    </td>
                    <td className="table-info-col">
                        {musteri.yillikGelir || ''}
                    </td>
                </tr>
                <tr>
                    <td className="table-label-col">
                        <h2 className="musteri-info-label">Telefon</h2>
                    </td>
                    <td className="table-info-col">
                        {musteri.telefon || ''}
                    </td>
                </tr>
                <tr>
                    <td className="table-label-col">
                        <h2 className="musteri-info-label">Adres</h2>
                    </td>
                    <td className="table-info-col">
                        {musteri.adres || ''}
                    </td>
                </tr>
                <tr>
                    <td className="table-label-col">
                        <h2 className="musteri-info-label">E-Posta</h2>
                    </td>
                    <td className="table-info-col">
                        {musteri.eposta || ''}
                    </td>
                </tr>
            </tbody>
        </table>
    )
}

function MusteriUrunler() {
    return (
        <>
            urunler
        </>
    )
}

class GorusmeTimeLine extends Component {
    render() {
        return (
            <GorusmeTimeLineWrapper>
                <div class="container mt-5 mb-5">
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <h4>Latest News</h4>
                            <ul class="timeline">
                                <li>
                                    <a target="_blank" href="https://www.totoprayogo.com/#">New Web Design</a>
                                    <a href="#" class="float-right">21 March, 2014</a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                                </li>
                                <li>
                                    <a href="#">21 000 Job Seekers</a>
                                    <a href="#" class="float-right">4 March, 2014</a>
                                    <p>Curabitur purus sem, malesuada eu luctus eget, suscipit sed turpis. Nam pellentesque felis vitae justo accumsan, sed semper nisi sollicitudin...</p>
                                </li>
                                <li>
                                    <a href="#">Awesome Employers</a>
                                    <a href="#" class="float-right">1 April, 2014</a>
                                    <p>Fusce ullamcorper ligula sit amet quam accumsan aliquet. Sed nulla odio, tincidunt vitae nunc vitae, mollis pharetra velit. Sed nec tempor nibh...</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </GorusmeTimeLineWrapper>

        )
    }
}
const GorusmeTimeLineWrapper = styled.div`
    ul.timeline {
        list-style-type: none;
        position: relative;
    }
    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul.timeline > li {
        margin: 20px 0;
        padding-left: 20px;
    }
    ul.timeline > li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }
`


export default class MusteriDetailPage extends Component {

    state = {
        musteri: {
            id: 1,
            adi: "Polisan",
            sektor: {
                adi: 'kimya'
            },
            calisanSayisi: 1253,
            yillikGelir: 1500000,
            telefon: '+90 532 651 11 11',
            adres: 'Kocaeli OSB',
            eposta: 'info@polisan.com.tr',
            web: 'www.polisan.com.tr',
            osb: 'kocaeli'
        }
    }

    render() {
        const { id } = this.props.match.params
        const { musteri } = this.state;
        return (
            <MusteriPageWrapper className="text-left">
                <Row>
                    <Col xs={12} md={3}>
                        <MusteriBilgi musteriID={id} />
                    </Col>
                    <Col xs={12} md={9} >
                        <Row className="mt-3">
                            <Col> <GorusmeNotlari /></Col>
                        </Row>
                        <Row className="mt-3">
                            <Col><Teklifler musteriID={id} /></Col>
                        </Row>
                        <Row className="mt-3">
                            <Col><OnayBekleyenTeklifler musteriID={id} /></Col>
                        </Row>
                        <Row className="mt-3">
                            <Col> <OncekiDestekler musteriID={id} /></Col>
                        </Row>
                        <Row className="mt-3">
                            <Col><Kisiler musteriID={id} /></Col>
                        </Row>
                        <Row className="mt-3">
                            <Col> <Danisman /></Col>
                        </Row>
                    </Col>
                </Row>
            </MusteriPageWrapper>
        )
    }
}

const MusteriPageWrapper = styled.div``

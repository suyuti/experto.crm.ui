import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
import { Form, Button } from 'react-bootstrap'
import {userService}  from '../Services/user.service'
import UserEklemeForm from "../Components/UserEklemeForm"


 


export default class UserPage extends Component {
    state = {
        columns: [
            {text: 'id',                dataField: '_id',            hidden: true},
            {text: 'Kullanıcı',         dataField: 'Adi',    },
            {text: 'Kullanıcı Adı',     dataField: 'KullaniciAdi'},
            {text: 'Email',             dataField: 'Email'}, 
            {text: 'Yönetici',          dataField: 'Ust' }, 
        ],
        data: [],
        modalOpen:false
    }
    componentDidMount() {
        userService.getAll().then(response => {
            this.setState({data: response.data})
        })
    }


    onNew=(data)=>{
        userService.create(data).then(response => {
            if(response.status!==200) return;   
            //this.state.data.push(data);
            var new_Data = this.state.data.concat(data) ;
            this.setState({data:new_Data});
        })
    }
    onUpdate=(data)=>{
        userService.update(data).then(response => {
            if(response.status!==200) return;
            console.log(data)
            var updated= this.state.data.map(item => {
                if (item._id == data._id) {

                  return data;
                } else {
                  return item;
                } 
            })  
            this.setState({data:updated }) ;
            this.setState({modalOpen:false }) ;
            console.log("modalOpen: false");
            
        })


    }
    onDelete=(data)=>{ 
        userService.remove(data).then(response => {
            if(response.status!==200) return;
            var newArray= this.state.data.filter((item)=>{
               return item._id!==data._id
            }) 
            this.state.data =newArray;
            this.setState({})
        })
    }

    render() {
        const {data, columns} = this.state
        return (<> 
            <ListSearchPage 
                title = "Kullanıcılar" 
                subtitle = "Kullanıcı Ekleme Formu" 
                columns={columns}
                linkurl='/user' 
                data={data} 
                ModelForm={UserEklemeForm} 
                striped 
                condensed 
                hover
                onNew = {this.onNew}
                onUpdate={this.onUpdate}
                onDelete={this.onDelete} 
                modalOpen={this.state.modalOpen}
                />
                </>
        )
    }
}

import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
import { Form, Button, Card, Container, Row, Col } from 'react-bootstrap'
import { musteriService } from '../Services/musteri.service'
import { destekService } from '../Services/destek.service'

import OncekiDesteklerForm from "../Components/OncekiDesteklerForm"

import { withRouter } from 'react-router-dom';

var currentUser = JSON.parse(localStorage.getItem("currentUser"));



class TeklifPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            musteri: {},
            oncekiDestekler: {},
            kisiler: [],
            odId: this.props.match.params.odId

        }
    }


    componentDidMount() {

        if (this.props.match.params.odId) {
            destekService.getById(this.props.match.params.odId).then((response) => {
                if (response.status !== 200) return; 
                response.data[0].Tarih = new Date(response.data[0].Tarih)
                this.setState({ oncekiDestekler: response.data[0] });
            }) 
        } 
    }


    onNew = () => {
        this.state.oncekiDestekler["Musteri"] = this.props.match.params.id;
        destekService.create(this.state.oncekiDestekler).then(response => {
            if (response.status !== 200) return;
            this.props.history.goBack();
        })
    }
    onUpdate = (data) => {
        destekService.update(this.state.oncekiDestekler).then(response => {
            if (response.status !== 200) return;
            this.props.history.goBack();
        })

    }
    onDelete = (data) => {
        // userService.remove(data).then(response => {
        //     if(response.status!==200) return;
        //     var newArray= this.state.data.filter((item)=>{
        //        return item._id!==data._id
        //     }) 
        //     this.state.data =newArray;
        //     this.setState({})
        // })
    }
    onHandleChangeModalData = (value) => e => {
        var tempData = this.state.oncekiDestekler;
        var data;
        if (e.target && e.target.type == "number") {
            data = parseInt(e.target.value);
        } else if (e.target) {
            data = e.target.value
        }
        tempData[value] = e.target ? data : e
        this.setState({ oncekiDestekler: tempData })
    }

    render() {
        return (<Card className="pb-5">
            <Container className="text-left">
                <Row>
                    <Col xs={12} className="p-3  " >
                        <Button variant="outline-secondary float-left"
                            onClick={() => {
                                this.props.history.goBack();
                            }}
                        >Go Back</Button>
                        <h1 className="text-center"> Onceki Destekler</h1>
                    </Col>
                </Row>
                <hr />
                <Row>
                    <Col xs={12}>
                        <OncekiDesteklerForm
                            onChangeHandler={this.onHandleChangeModalData}
                            tempModalData={this.state.oncekiDestekler}
                        />
                    </Col>
                    <Col xs={12}>
                        {this.state.odId ?
                            <button type="button" className="btn btn-outline-primary  btn-block" size="sm" onClick={this.onUpdate}>
                                Guncelle
                    </button> :
                            <button type="button" className="btn btn-outline-primary  btn-block" size="sm" onClick={this.onNew}>
                                Ekle
                    </button>
                        }
                        {/* <button type="button" className="btn   btn-sm ml-2 ts-buttom" size="sm" onClick={}>
                    Delete
                </button> */}
                    </Col>
                    {/* <Col>
                        {JSON.stringify(this.state.oncekiDestekler)}
                    </Col> */}
                </Row>
            </Container>


        </Card>
        )
    }
}


export default withRouter(TeklifPage)
import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
import { Form, Button } from 'react-bootstrap'
import {musteriService}  from '../Services/musteri.service'
import MusteriAdayEklemeForm from "../Components/MusteriAdayEklemeForm"





export default class MusterilerPage extends Component {
    state = {
        columns: [
            {text: 'id',                dataField: '_id',           hidden: true},
            {text: 'Firma Adı',         dataField: 'FirmaUnvani',   sort: true, link: '/musteri'},
            {text: 'Firma Telefonu',    dataField: 'Telefon'},
            {text: 'Firma İli',         dataField: 'il'},
            {text: 'Firma Web Sitesi',  dataField: 'url',           link: true},
            {text: 'Firma Sektörü',     dataField: 'FirmaSektoru',  link: '/sektor'},
        ],
        data: []
    }
    componentDidMount() {
        musteriService.getAll().then(response => {
            this.setState({data: response.data})
        })
    }

    onNew=(data)=>{ 
        musteriService.create(data).then(response=>{  
            this.setState({data:[ ...this.state.data   ,response.data]}) 
            
        }) 
        return true
    }
    onUpdate=()=>{

    }
    onDelete=(data)=>{

        musteriService.remove(data).then(response=>{  
            if(response.status!==200) return;
            var newData= this.state.data .filter((item)=>{
                return item._id!==data._id
            })
            this.setState({data:[ ...newData]}) 
            
        }) 

    }

    render() {
        const {data, columns} = this.state
        return (
            <ListSearchPage 
                title = "Müşteriler"
                subtitle = "Müşteri Ekleme Formu" 
                columns={columns}
                linkurl='/musteri' 
                data={data} 
                ModelForm={MusteriAdayEklemeForm} 
                striped 
                condensed 
                hover
                onNew = {this.onNew}
                onUpdate={this.onUpdate}
                onDelete={this.onDelete}
                />
        )
    }
}

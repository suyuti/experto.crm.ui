import React, { Component } from 'react'
import ListSearchPage from '../Components/ListSearchPage'
import { Form, Button, Card, Container, Row, Col } from 'react-bootstrap'
import { musteriService } from '../Services/musteri.service'
import { kisiService } from '../Services/kisi.service'

import TeklifEklemeForm from "../Components/TeklifEklemeForm"

import { withRouter } from 'react-router-dom';
import { urunService } from '../Services/urun.service'

var currentUser = JSON.parse(localStorage.getItem("currentUser"));

class TeklifPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            musteri: {},
            teklif: {},
            kisiler: [],
            teklifID: this.props.match.params.teklifID,
            hata : {},
            teklifValid : false
        }
    }

    componentDidMount() {
        musteriService.getTeklifById(this.props.match.params.teklifID).then((response) => {
            if (response.status !== 200) return;
            response.data[0].KapanisTarihi = new Date(response.data[0].KapanisTarihi)
            this.setState({ teklif: response.data[0] });
        })

        musteriService.getById(this.props.match.params.id).then(response => {
            this.setState({ musteri: response.data[0] });
            this.onHandleChangeModalData("Firma")(response.data[0]._id);
            this.onHandleChangeModalData("MusteriTemsilcisi")(currentUser._id);
        });

        kisiService.getAll().then(response => {
            this.setState({ kisiler: response.data })
        });

    }

    teklifiKontrolEt = (teklif) => {
        var indirimOrani = currentUser.indirimOrani || 10
        var seciliUrun = this.state.seciliUrun;
        if (seciliUrun) {
            var hata = this.state.hata;
            var teklifValid = true
            if (seciliUrun.OnOdemeTutari) {
                hata['OnOdemeValid']     = (this.state.teklif.OnOdemeTutari <= seciliUrun.OnOdemeTutari) && (this.state.teklif.OnOdemeTutari >= seciliUrun.OnOdemeTutari * ((100-indirimOrani) / 100))
                teklifValid = teklifValid && hata.OnOdemeValid
            }
            if (seciliUrun.SabitTutar) {
                hata['SabitOdemeValid']  = (this.state.teklif.SabitTutar <= seciliUrun.SabitTutar) && (this.state.teklif.SabitTutar >= seciliUrun.SabitTutar * ((100-indirimOrani) / 100))                
                teklifValid = teklifValid && hata.SabitOdemeValid
            }
            if (seciliUrun.BasariTutari) {
                hata['BasariOdemeValid'] = (this.state.teklif.BasariTutari <= seciliUrun.BasariTutari) && (this.state.teklif.BasariTutari >= seciliUrun.BasariTutari * ((100-indirimOrani) / 100))                
                teklifValid = teklifValid && hata.BasariOdemeValid
            }
            if (seciliUrun.RaporBasiOdemeTutari) {
                hata['RaporOdemeValid']  = (this.state.teklif.RaporBasiOdemeTutari <= seciliUrun.RaporBasiOdemeTutari) && (this.state.teklif.RaporBasiOdemeTutari >= seciliUrun.RaporBasiOdemeTutari * ((100-indirimOrani) / 100))                
                teklifValid = teklifValid && hata.RaporOdemeValid
            }
            if (seciliUrun.YuzdeTutari) {
                hata['YuzdeOdemeValid']  = (this.state.teklif.YuzdeTutari <= seciliUrun.YuzdeTutari) && (this.state.teklif.YuzdeTutari >= seciliUrun.YuzdeTutari * ((100-indirimOrani) / 100))                
                teklifValid = teklifValid && hata.YuzdeOdemeValid
            }

            this.setState({hata: hata, teklifValid: teklifValid})
        }
    }

    onNew = () => {
        musteriService.newTeklifler(this.state.teklif).then(response => {
            if (response.status !== 200) return;
            this.props.history.goBack();
        })
    }
    onUpdate = (data) => {
        musteriService.updateTeklif(this.state.teklif).then(response => {
            if (response.status !== 200) return;
            this.props.history.goBack();
        })
    }
    onDelete = (data) => {
        // userService.remove(data).then(response => {
        //     if(response.status!==200) return;
        //     var newArray= this.state.data.filter((item)=>{
        //        return item._id!==data._id
        //     }) 
        //     this.state.data =newArray;
        //     this.setState({})
        // })
    }

    onHandleChangeModalData = (value) => e => {
        var tempData = this.state.teklif;
        var data;
        if (e.target && e.target.type == "number") {
            data = parseInt(e.target.value);
        } else if (e.target) {
            data = e.target.value
        }
        tempData[value] = e.target ? data : e

        this.teklifiKontrolEt(tempData)

        this.setState({ teklif: tempData })
    }

    onUrunSelected = (urun) => {
        this.setState({seciliUrun: urun})
        var hata = this.state.hata;

        hata['OnOdemeValid']     = true
        hata['SabitOdemeValid']  = true
        hata['RaporOdemeValid']  = true
        hata['BasariOdemeValid'] = true
        hata['YuzdeOdemeValid']  = true
            
        this.setState({hata: hata, teklifValid: false})
}

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col">
                        <Card className="pb-5">
                            <Container className="text-left" fluid>
                                <Row>
                                    <Col xs={12} className="p-3" >
                                        <Button variant="outline-secondary float-left mr-4"
                                            onClick={() => {
                                                this.props.history.goBack();
                                            }}
                                        >Geri</Button>
                                        <h2 className="text-left">Teklif Hazırlama</h2>
                                    </Col>
                                </Row>
                                <hr />
                                <Row>
                                    <Col xs={6} className="p-3  " >
                                        <h4 className="text-left"><u> {`${this.state.musteri.FirmaUnvani}`.toUpperCase()}</u></h4>
                                        {this.state.musteri.Telefon}<br></br>

                                    </Col>
                                    <Col xs={6} className="p-3  " >
                                        <h5 className="text-left">ilgili Kişi:</h5>
                                        <select class="custom-select custom-select-lg mb-3"
                                            value={this.state.teklif.IlgiliKisi}
                                            onChange={this.onHandleChangeModalData("IlgiliKisi")}>
                                            <option   >select</option>
                                            {this.state.kisiler.map((item) => {
                                                return <option value={item._id}>{item.Adi}</option>
                                            })}
                                        </select>
                                    </Col>
                                </Row>
                                <hr />
                                <Row>
                                    <Col xs={12}>
                                        <TeklifEklemeForm
                                            onChangeHandler={this.onHandleChangeModalData}
                                            tempModalData={this.state.teklif}
                                            onUrunSelected = {this.onUrunSelected}
                                            error = {this.state.hata}
                                        />
                                    </Col>
                                    <Col xs={12}>
                                        <hr />

                                        {this.state.teklifID ?
                                            <button disabled={!this.state.teklifValid} type="button" className="btn btn-primary float-right" size="sm" onClick={this.onUpdate}>
                                                Guncelle
                                            </button>
                                            :
                                            <button disabled={!this.state.teklifValid} type="button" className="btn btn-success float-right" onClick={this.onNew}>
                                                Teklif Oluştur
                                            </button>
                                        }
                                    </Col>
                                </Row>
                            </Container>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(TeklifPage)
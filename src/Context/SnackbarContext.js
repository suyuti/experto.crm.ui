import React, { Component, createContext } from "react";

// const { Provider, Consumer } = createContext();

const SnackbarContext = React.createContext();

class SnackbarProvider extends Component {
    state = {
        open: false,
        message: "",
        type: 200,
        Snackbar: (state, message, type) => {
            this.setState({
                open: state,
                message: message ? message : "Mesaj yok",
                type: type ? type : 200,
            });
        }
    };

    render() {
        return (
            <SnackbarContext.Provider
                value={{
                    open: this.state.open,
                    message: this.state.message,
                    type: this.state.type,
                    Snackbar: this.state.Snackbar
                }}
            >
                {this.props.children}
            </SnackbarContext.Provider>
        );
    }
}
 
const SnackbarConsumer = SnackbarContext.Consumer;

 
export { SnackbarProvider,SnackbarConsumer};
export default SnackbarContext;
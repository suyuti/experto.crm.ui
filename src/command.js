String.prototype.gunAyYil = function() {
    var d= new Date(this); 
    //gun ay yıl
    return `${d.getDate()}.${d.getMonth()+1}.${d.getFullYear()} `
  };
  
Date.prototype.monthDays= function(){
    var d= new Date(this.getFullYear(), this.getMonth()+1, 0);
    return d.getDate();
}

export const  userDashboard=[
  {id:0,name: "Default"},
  {id:1,name: "Teknik"},
  {id:2,name: "Satış"},
  {id:3,name: "Mali"},
  {id:4,name: "Yönetim"},  
]
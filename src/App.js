import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import './App.css';
// import { Container, Navbar, Button, NavDropdown, Nav, Form, FormControl } from 'react-bootstrap';

import HeaderComponent from './Components/Layout/Header';
// import FooterComponent from './Components/Layout/Footer';
import ContentComponent from './Components/Layout/Content';
import SidebarComponent from './Components/Layout/Sidebar';
import LoginPages from './Pages/LoginPages';

// import { BrowserRouter as Router } from "react-router-dom";
// import command from './command';


import {SnackbarProvider} from './Context/SnackbarContext';


function App() {
    var sidebarLocalStorage = localStorage.getItem("expertoCrmSidebar");
    const [isOpen, setOpen] = useState(sidebarLocalStorage == 'true')
    var currentUser=localStorage.getItem("currentUser");
    const toggle = () => {
        localStorage.setItem("expertoCrmSidebar", !isOpen)
        setOpen(!isOpen)
    }

    if( JSON.stringify(currentUser)== "{}" || !currentUser){
        return(
            <LoginPages/>
        )
    }
    return (


        <SnackbarProvider>  
            <div className="App d-flex w-100 flex-column" style={{ height: "100vh" }}>
                <HeaderComponent user={JSON.parse(currentUser)} />
                <div className="d-flex flex-row" style={{ flex: "1 0 auto" }}>
                    <SidebarComponent isOpen={isOpen} toggle={toggle} />
                    <ContentComponent />
                </div>
            </div>
        </SnackbarProvider>
    );
}

export default App;

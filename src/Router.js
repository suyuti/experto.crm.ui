import React from 'react'; 
import { Switch, Route } from 'react-router-dom'; 
import DashboardDefaultPages from './Pages/DashboardDefaultPages'
import DashboardTeknikPages from './Pages/DashboardTeknikPages'
import DashboardSatisPages from './Pages/DashboardSatisPages' 
import DashboardMaliPages from './Pages/DashboardMaliPages' 
import DashboardYonetimPages from './Pages/DashboardYonetimPages'  
 
import FirmaPage from './Pages/FirmaPage';
import KisiPage from './Pages/KisiPage';
import MusteriPage from './Pages/MusteriDetailPage';
import SektorListPage from './Pages/SektorListPage';
import MusterilerPage from './Pages/MusterilerPage';
import UserPage from './Pages/UserPage';
import TeklifPage from './Pages/TeklifPage';
import KisilerPage from './Pages/KisilerPage';
import MusteriUpdatePage from './Pages/MusteriUpdatePage';
import OncekiDestekler from './Pages/OncekiDestekler';
import UrunlerPage from './Pages/UrunlerPage';
var currentUser=JSON.parse(localStorage.getItem("currentUser")) ;



const HomePagesType = {
  0: DashboardDefaultPages,
  1: DashboardTeknikPages,
  2: DashboardSatisPages,
  3: DashboardMaliPages,
  4: DashboardYonetimPages,
};


   


export default props => ( 
      <Switch> 
        <Route exact path="/" component={HomePagesType[currentUser.Dashboard?currentUser.Dashboard:0]  } /> 
        <Route exact path="/urunler"      component={UrunlerPage} /> 
        <Route exact path="/home" component={() => "home" } />
        <Route exact path="/musteri"      component={MusterilerPage} /> 
        <Route exact path="/musteri/:id"  component={MusteriPage} />
        <Route exact path="/musteri/:mid/update"  component={MusteriUpdatePage} />
        <Route exact path="/musteri/:id/teklif/:teklifID" component={TeklifPage} />
        <Route exact path="/musteri/:id/teklif" component={TeklifPage} />
        <Route exact path="/musteri/:id/oncekidestekler" component={OncekiDestekler} />
        <Route exact path="/musteri/:id/oncekidestekler/:odId" component={OncekiDestekler} />
        <Route exact path="/musteri/:id/kisiler" component={KisilerPage} />
        <Route exact path="/musteri/:id/kisiler/:kid" component={KisilerPage} />
        <Route exact path="/firma" component={FirmaPage} />
        <Route exact path="/sektor" component={SektorListPage} />
        <Route exact path="/user" component={UserPage} />
        <Route exact path="/kisi" component={KisiPage} />
        <Route exact path="/faq" component={() => "FAQ" } />
      </Switch> 
)

import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap'
import { urunService } from '../Services/urun.service'
import  DatePicker  from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

var minDate=new Date();;
var maxDate=new Date(); 
maxDate.setDate(maxDate.getDate()+ 15);
var currentUser =JSON.parse(localStorage.getItem("currentUser")); 

class MusteriAdayEklemeForm extends Component {
    
    state = {
        urun: [],
        selectUrun: {} 
    }
    constructor(props) {
        super(props);
        

    } 
    componentDidMount() {
          
 
        urunService.getAll().then(response => {
            this.setState({ urun: response.data })
        });
    }
   


    handleSelectUrun = (e) => {
        var id = e.target.value;
        var index = this.state.urun.findIndex((item) => item._id == id)
        this.props.onChangeHandler("OnOdemeTutari")(this.state.urun[index].OnOdemeTutari);
        this.props.onChangeHandler("RaporBasiOdemeTutari")(this.state.urun[index].RaporBasiOdemeTutari);
        this.props.onChangeHandler("YuzdeTutari")(this.state.urun[index].YuzdeTutari);
        this.props.onChangeHandler("SabitTutar")(this.state.urun[index].SabitTutar);
        this.props.onChangeHandler("BasariTutari")(this.state.urun[index].BasariTutari);

        this.setState({ selectUrun: this.state.urun[index] });
    }
 ////sonradan düzenlenecek
      maxLengthCheck=(value)=> { 
        //   tempModalData.BasariTutari
        // if (this.value > this.max){
        //     this.value = this.max
        // }

     }

    render() {
        const { onChangeHandler, tempModalData } = this.props
        return (
            <Form className="row"> 
                <Form.Group className="col-6">
                    <Form.Label>Adi</Form.Label>
                    <Form.Control type="text"
                        placeholder="Adi"
                        onChange={onChangeHandler("Adi")}
                        value={tempModalData.Adi}  
                        onInput={this.maxLengthCheck(tempModalData.Adi)}
                    />
                </Form.Group>   
                <Form.Group className="col-6">
                    <Form.Label>Tarih</Form.Label>
                     <br/>
                    <DatePicker  
                        minDate={minDate}
                        maxDate={maxDate}
                        onChange={onChangeHandler("Tarih")}
                        selected={tempModalData.Tarih}
                        placeholderText="Lütfen Tarih seçiniz"
                        />
                </Form.Group> 
                <Form.Group className="col-6">
                    <Form.Label>Durum</Form.Label> 
                    <select class="custom-select custom-select-lg"
                        value={tempModalData.Durum}
                        onChange={onChangeHandler("Durum")}>
                        <option   >select</option> 
                        <option style={{ padding: 8 }} value={200}>Başarılı</option>  
                        <option style={{ padding: 8 }} value={1}>Devam ediyor</option> 
                        <option style={{ padding: 8 }} value={99}>Başarısız</option>  
                    </select>

                </Form.Group>   

                

               
            </Form>
        )
    }
}



export default MusteriAdayEklemeForm;
import React, { Component } from 'react';
import { Form, Button, Col } from 'react-bootstrap'
import { userDashboard} from '../command'

class MusteriAdayEklemeForm extends Component {
  constructor(props) {
    super(props);

  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  render() {
    const { onChangeHandler, tempModalData } = this.props
    return (
      <Form id="kullanici-form">
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Adı</Form.Label>
            <Form.Control type="text"
              placeholder="Adı"
              onChange={onChangeHandler("Adi")}
              value={tempModalData.Adi}
            />
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Soyadı</Form.Label>
            <Form.Control type="text"
              placeholder="Soyadı"
              onChange={onChangeHandler("Soyadi")}
              value={tempModalData.Soyadi}
            />
          </Form.Group>
        </Form.Row>
        <hr />
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Kullanıcı Adı</Form.Label>
            <Form.Control type="text"
              placeholder="Kullanıcı Adı"
              onChange={onChangeHandler("KullaniciAdi")}
              value={tempModalData.KullaniciAdi}
            />
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Şifre</Form.Label>
            <Form.Control type="text"
              placeholder="Kullanıcı Şifre"
              onChange={onChangeHandler("Password")}
              value={tempModalData.Password}
            />
          </Form.Group>

          <Form.Group as={Col} className="col-12">
            <Form.Label>Email</Form.Label>
            <Form.Control type="text"
              placeholder="Kullanıcı Email"
              onChange={onChangeHandler("Email")}
              value={tempModalData.Email}
            />
          </Form.Group>

        </Form.Row>
        <hr />
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Yönetici</Form.Label>
            <Form.Control type="text"
              placeholder="Yönetici/Yetkili"
              onChange={onChangeHandler("Ust")}
              value={tempModalData.Ust}
            />
          </Form.Group>
        </Form.Row>
        <hr />
        {/* <Form.Group className="col-12">
                    <Form.Label>Ast</Form.Label>
                    <Form.Control type="text" 
                        placeholder="Ast" 
                        onChange={onChangeHandler("Ast")}
                        value={tempModalData.Ast}
                     />  
                </Form.Group> */}
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>İndirim Oranı</Form.Label>
            <Form.Control type="text"
              placeholder="İndirim Oranı"
              onChange={onChangeHandler("IndirimOrani")}
              value={tempModalData.IndirimOrani}
            />
          </Form.Group> 
          <Form.Group as={Col}>
            <Form.Label>Dashboard</Form.Label>
            <select class="custom-select custom-select-lg"
              value={Number(tempModalData.Dashboard)}
              onChange={onChangeHandler("Dashboard")}> 
              {userDashboard.map((item)=>{
                return <option style={{ padding: 8 }} value={Number(item.id)}>{item.name}</option>
              })}
               
            </select>
          </Form.Group>
        </Form.Row>

      </Form>
    )
  }
}



export default MusteriAdayEklemeForm;
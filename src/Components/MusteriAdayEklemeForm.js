import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap'
import { ilService } from '../Services/il.service' 
const ilIlce = require('../il-ilce.json')


class MusteriAdayEklemeForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "iller": [],
            ilceler: [],
            ilIlce: ilIlce,
            seciliIl: undefined
        };
    }

    componentWillMount() {
        ilService.getAll().then(response => {
            this.setState({ iller: response.data })
            console.log(this.state.iller)
        })
    }

    componentDidMount() {


    }
    getIlce=(e)=>{
      ilService.getIlce(e.target.value).then(response=>{
        this.setState({ilceler:response.data}) 
        console.log(this.state.ilceler)
       })

    }

    render() {
        const { onChangeHandler } = this.props
        return (
            <div>
                <form>
                    <div className="form-row">
                        <div className="form-group col">
                            <label for="firma-unvani">Ünvan</label>
                            <input type="text" className="form-control" placeholder="Firma Ünvanı" id="firma-unvani" onChange={onChangeHandler("FirmaUnvani")}></input>
                        </div>
                    </div>
                    <hr />
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="firma-il">İl</label>
                            <select id="firma-il" className="custom-select" onChange={(e) => { this.setState({ seciliIl: e.target.value }); onChangeHandler("il")(e) }} >
                                <option hidden selected>Firma İli</option>
                                {
                                    this.state.ilIlce.map(item => {
                                        return (<option value={item.il}>{item.il}</option>)
                                    })
                                }
                            </select>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="firma-ilce">İlçe</label>
                            <select id="firma-ilce" className="custom-select" onChange={onChangeHandler("ilce")} >
                                <option hidden selected>Firma İlçe</option>
                                {
                                    this.state.seciliIl &&
                                    this.state.ilIlce.find(i => i.il == this.state.seciliIl).ilceleri.map(item => {
                                        return (<option >{item}</option>)
                                    })
                                }
                            </select>
                        </div>
                    </div>
                    <hr />
                    <div className="form-row">
                        <div className="form-group col">
                            <label for="firma-telefonu">Telefon</label>
                            <input type="text" className="form-control" placeholder="Firma Telefonu" id="firma-telefonu" onChange={onChangeHandler("Telefon")} data-inputmask="'mask': '99-9999999'" ></input>
                        </div>
                    </div>
                </form>
            </div>)
    }
}


export default MusteriAdayEklemeForm;
import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap'
import { urunService } from '../Services/urun.service'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

var minDate = new Date();;
var maxDate = new Date();
maxDate.setDate(maxDate.getDate() + 15);
var currentUser = JSON.parse(localStorage.getItem("currentUser"));

class TeklifEklemeForm extends Component {
    state = {
        urunler: [],
        seciliUrun: {},
        projeMaxSure: 32,
        teklif : {},

        teklif_fiyatlar: {},

        isValid: false,

        onOdemeTutari_is_valid  : false,
        sabitTutar_is_valid     : false,
        basariTutar_is_valid    : false,
        raporBasiTutar_is_valid : false,
        yuzdeTutar_is_valid     : false
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        urunService.getAll().then(response => {
            this.setState({ urunler: response.data })
        });
    }

    handleSelectUrun = (e) => {
        var id = e.target.value;
        var index = this.state.urunler.findIndex((item) => item._id == id)
        if (this.state.urunler[index].OnOdemeTutari != undefined) {
            this.props.onChangeHandler("OnOdemeTutari")(this.state.urunler[index].OnOdemeTutari);
        }
        if (this.state.urunler[index].RaporBasiOdemeTutari != undefined) {
            this.props.onChangeHandler("RaporBasiOdemeTutari")(this.state.urunler[index].RaporBasiOdemeTutari);
        }
        if (this.state.urunler[index].RaporBasiOdemeTutari != undefined) {
            this.props.onChangeHandler("RaporBasiOdemeTutari")(this.state.urunler[index].RaporBasiOdemeTutari);
        }
        if (this.state.urunler[index].SabitTutar != undefined) {
            this.props.onChangeHandler("SabitTutar")(this.state.urunler[index].SabitTutar);
        }
        if (this.state.urunler[index].BasariTutari != undefined) {
            this.props.onChangeHandler("BasariTutari")(this.state.urunler[index].BasariTutari);
        }

        this.setState({ seciliUrun: this.state.urunler[index] });
        this.props.onUrunSelected(this.state.urunler[index])
    }
    ////sonradan düzenlenecek
    maxLengthCheck = (value) => {
        //   tempModalData.BasariTutari
        // if (this.value > this.max){
        //     this.value = this.max
        // }

    }

    render() {
        const { onChangeHandler, tempModalData, error } = this.props
        const { seciliUrun } = this.state
        console.log(error)
        return (
            <div>
                <div className="container-fluid">
                    <form novalidate className= {this.state.isValid ? "was-validated": "needs-validation"}>
                        <div className="form-group row">
                            <label for="urunler-list" className="col-md-2 col-form-label mr-0">Ürünler</label>
                            <div className="col-md-10">
                                <select
                                    id="urunler-list"
                                    className="custom-select custom-select-lg mb-0"
                                    onChange={(e) => {
                                        this.handleSelectUrun(e);
                                        onChangeHandler("Urun")(e);
                                    }}>
                                    <option>Bir ürün seçin ...</option>
                                    {this.state.urunler.map((item) => {
                                        return <option style={{ padding: 8 }} value={item._id}>{item.Adi}</option>
                                    })}
                                </select>
                            </div>
                        </div>

                        {
                            seciliUrun != undefined
                            && seciliUrun.OnOdemeTutari != undefined
                            &&
                            <div className="form-group row">
                                <label 
                                    for="on-odeme-tutari" 
                                    className="col-md-2 col-form-label mr-0"
                                    title="Ön ödeme olarak alınacak tutar.">Ön ödeme tutarı (TL)</label>
                                <div className="col-md-10">
                                    <input
                                        className= {`form-control ${error.OnOdemeValid ? 'is-valid' : 'is-invalid'}`}
                                        id="on-odeme-tutari"
                                        type="text"
                                        placeholder="Ön ödeme tutarı"
                                        onChange={onChangeHandler("OnOdemeTutari")}
                                        value={tempModalData.OnOdemeTutari}
                                    />
                                    {error.OnOdemeValid ? 
                                        ''
                                    :
                                        <div className="invalid-feedback">Tutar dogru degil</div>
                                    }
                                </div>
                            </div>
                        }

                        {
                            seciliUrun != undefined
                            && seciliUrun.RaporBasiOdemeTutari != undefined
                            &&
                            <div className="form-group row">
                                <label 
                                    for="raporbasi-odeme-tutari" 
                                    className="col-md-2 col-form-label mr-0 is-valid"
                                    title="Her bir rapor icin odeme"
                                    >Rapor başına ödeme tutarı (TL)</label>
                                <div className="col-md-10">
                                    <input
                                        className= {`form-control ${error.RaporOdemeValid ? 'is-valid' : 'is-invalid'}`}
                                        id="raporbasi-odeme-tutari"
                                        type="text"
                                        placeholder="Rapor başına ödeme tutarı"
                                        onChange={onChangeHandler("RaporBasiOdemeTutari")}
                                        value={tempModalData.RaporBasiOdemeTutari}
                                    />
                                    {error.RaporOdemeValid ? 
                                        ''
                                    :
                                        <div className="invalid-feedback">Tutar dogru degil</div>
                                    }
                                </div>
                            </div>
                        }

                        {
                            seciliUrun != undefined
                            && seciliUrun.SabitTutar != undefined
                            &&
                            <div className="form-group row">
                                <label 
                                    for="sabit-tutar" 
                                    className="col-md-2 col-form-label mr-0"
                                    title="Sabit odeme">Sabit ödeme tutarı (TL)</label>
                                <div className="col-md-10">
                                    <input
                                        className= {`form-control ${error.SabitOdemeValid ? 'is-valid' : 'is-invalid'}`}
                                        id="sabit-tutar"
                                        type="text"
                                        placeholder="Sabit ödeme tutarı"
                                        onChange={onChangeHandler("SabitTutar")}
                                        value={tempModalData.SabitTutar}
                                    />
                                    {error.SabitOdemeValid ? 
                                        ''
                                    :
                                        <div className="invalid-feedback">Tutar dogru degil</div>
                                    }
                                </div>
                            </div>
                        }

                        {
                            seciliUrun != undefined
                            && seciliUrun.BasariTutari != undefined
                            &&
                            <div className="form-group row">
                                <label 
                                    for="basari-tutari" 
                                    className="col-md-2 col-form-label mr-0"
                                    title="Basari tutari">Başarı tutarı (TL)</label>
                                <div className="col-md-10">
                                    <input
                                        className= {`form-control ${error.BasariOdemeValid ? 'is-valid' : 'is-invalid'}`}
                                        id="basari-tutari"
                                        type="text"
                                        placeholder="Başarı tutarı"
                                        onChange={onChangeHandler("BasariTutari")}
                                        value={tempModalData.BasariTutari}
                                    />
                                    {error.BasariOdemeValid ? 
                                        ''
                                    :
                                        <div className="invalid-feedback">Tutar dogru degil</div>
                                    }
                                </div>
                            </div>
                        }

                        {
                            seciliUrun != undefined
                            && seciliUrun.YuzdeTutari != undefined
                            &&
                            <div className="form-group row">
                                <label 
                                    for="yuzde-tutari" 
                                    className="col-md-2 col-form-label mr-0"
                                    title="Yuzde tutari">Yüzde tutarı (TL)</label>
                                <div className="col-md-10">
                                    <input
                                        className="form-control"
                                        id="yuzde-tutari"
                                        type="text"
                                        placeholder="Yüzde tutarı"
                                        onChange={onChangeHandler("YuzdeTutari")}
                                        value={tempModalData.YuzdeTutari}
                                    />
                                </div>
                            </div>
                        }
                        <hr />
                        <div className="form-group row">
                            <label 
                                for="proje-suresi" 
                                className="col-md-2 col-form-label mr-0"
                                title="Proje kac ay surecek">Proje süresi (ay)</label>
                            <div className="col-md-10">
                                <select className="form-control"
                                    id="proje-suresi"
                                    onChange={onChangeHandler("ProjeSure")}>
                                    {
                                        Array(this.state.projeMaxSure).fill().map((_, i) => {
                                            return (<option>{i + 1}</option>)
                                        })
                                    }
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div className="form-group row">
                            <label 
                                for="teklif-kapanis-tarihi" 
                                className="col-md-2 col-form-label mr-0"
                                title="Teklif ne kadar sure ile gecerli"
                                >Teklif Kapanış Tarihi</label>
                            <div className="col-md-10">
                                <DatePicker
                                    id="teklif-kapanis-tarihi" 
                                    className="form-control"
                                    minDate={minDate}
                                    maxDate={maxDate}
                                    onChange={onChangeHandler("KapanisTarihi")}
                                    selected={tempModalData.KapanisTarihi}
                                    placeholderText="Lütfen Tarih seçiniz"
                                />
                            </div>
                        </div>

                        <hr />

                        <div className="form-group row">
                            <label 
                                for="ilave-sartlar" 
                                className="col-md-2 col-form-label mr-0"
                                title=" Varsa ilave sartlar">İlave şartlar ve koşullar</label>
                            <div className="col-md-10">
                                <textarea className="form-control" type="text" as="textarea" rows="3"
                                    placeholder="İlave Şartlar ve Koşullar"
                                    onChange={onChangeHandler("SartlarVeKosullar")}
                                    value={tempModalData.SartlarVeKosullar}
                                    min="1" max="999"
                                    onInput={this.maxLengthCheck(tempModalData.SartlarVeKosullar)}
                                />
                            </div>
                        </div>

                        <hr />
                        <div className="form-group row">
                            <label for="iadeliMi" title="Iade var mi?" className="col-md-2 col-form-label mr-0">İadeli mi?</label>
                            <div className="col-md-10">
                                <label className="switch ">
                                    <input className="success"
                                    value={tempModalData.IadeliMi == "true" ? "false" : "true"}
                                    onChange={onChangeHandler("IadeliMi")}
                                    type="checkbox"
                                    id={`iadeliMi`}
                                    label="İadeli mi?"
                                />
                                    <span className="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}



export default TeklifEklemeForm;
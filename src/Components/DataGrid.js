import React, { Component } from 'react'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
const { SearchBar } = Search;

export default class DataGrid extends Component {
    render() {
        const {columns, data, ...tableprops} = this.props
        return(
            <ToolkitProvider
                keyField="id"
                data={ data }
                columns={ columns }
                {...tableprops}
                search
            >
                {
                    props => (
                    <div>
                        <SearchBar { ...props.searchProps } />
                        <hr />
                        <BootstrapTable
                        { ...props.baseProps }
                        />
                    </div>
                    )
                }
            </ToolkitProvider>
        )
        /*return (
            <div>
                <BootstrapTable
                    tableHeaderClass = 'table-header-class' 
                    tableBodyClass = 'table-body-class'
                    containerClass = 'table-container-class'
                    tableContainerClass = 'table-table-container-class'
                    headerContainerClass = 'table-header-container-class'
                    bodyContainerClass = 'table-body-container-class'

                    {...tableprops}
                    height="70vh" 
                    hover 
                    condensed 
                    //insertRow 
                    //exportCSV
                    //search={ true } 
                    options={ { noDataText: 'Veri yok' } }>
                    {columns.map(col => {

                        var isKey = false
                        if (col.hasOwnProperty('isKey')) {
                            isKey = col.isKey
                        }
                        return(
                            <TableHeaderColumn isKey={col.title == 'id'} dataField={col.field}>{col.title}</TableHeaderColumn>
                        )
                    })}
                    <TableHeaderColumn isKey={true} dataField="id" dataFormat={this.buttonFormatter}>Buttons</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )*/
    }
}

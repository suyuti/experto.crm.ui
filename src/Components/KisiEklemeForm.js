import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap'
import { urunService } from '../Services/urun.service'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

var minDate = new Date();;
var maxDate = new Date();
maxDate.setDate(maxDate.getDate() + 15);
var currentUser = JSON.parse(localStorage.getItem("currentUser"));

class MusteriAdayEklemeForm extends Component {

    state = {
        urun: [],
        selectUrun: {}
    }
    constructor(props) {
        super(props);


    }
    componentDidMount() {


        urunService.getAll().then(response => {
            this.setState({ urun: response.data })
        });
    }
    componentWillMount() {

    }




    handleSelectUrun = (e) => {
        // var id = e.target.value;
        // var index = this.state.urun.findIndex((item) => item._id == id)
        // this.props.onChangeHandler("OnOdemeTutari")(this.state.urun[index].OnOdemeTutari);
        // this.props.onChangeHandler("RaporBasiOdemeTutari")(this.state.urun[index].RaporBasiOdemeTutari);
        // this.props.onChangeHandler("YuzdeTutari")(this.state.urun[index].YuzdeTutari);
        // this.props.onChangeHandler("SabitTutar")(this.state.urun[index].SabitTutar);
        // this.props.onChangeHandler("BasariTutari")(this.state.urun[index].BasariTutari);

        // this.setState({ selectUrun: this.state.urun[index] });
    }

    maxLengthCheck = (value) => {


    }

    render() {
        const { onChangeHandler, tempModalData } = this.props
        return (<>
            <Form className="row">
                <Form.Group className="col-md-6 col-xs-12">
                    <Form.Label>Adi</Form.Label>
                    <Form.Control type="text"
                        placeholder="Adi"
                        onChange={onChangeHandler("Adi")}
                        value={tempModalData.Adi}
                    />
                </Form.Group>
                <Form.Group className="col-md-4 col-xs-12">
                    <Form.Label>Soyadi</Form.Label>
                    <Form.Control type="text"
                        placeholder="Soyadi"
                        onChange={onChangeHandler("Soyadi")}
                        value={tempModalData.Soyadi}
                    />
                </Form.Group>
                <Form.Group className="col-md-2 col-xs-12">
                    <Form.Label>Cinsiyet</Form.Label>
                    <select class="custom-select custom-select-lg "
                        value={tempModalData.Cinsiyet}
                        onChange={onChangeHandler("Cinsiyet")}>
                        <option value="">select</option>
                        <option value="Kadın">Kadın</option>
                        <option value="Erkek">Erkek</option>
                    </select>
                </Form.Group>
                <Form.Group className="col-md-2 col-xs-12">
                    <Form.Label>Tipi</Form.Label>
                    <select class="custom-select custom-select-lg "
                        value={tempModalData.Type}
                        onChange={onChangeHandler("Type")}>
                        <option value="">select</option>
                        <option value="1">Yonetim</option>
                        <option value="2">Mali</option>
                        <option value="3">Teknik</option>
                    </select>
                </Form.Group>
                <Form.Group className="col-md-10 col-xs-12">
                    <Form.Label>Unvani</Form.Label>
                    <Form.Control type="text"
                        placeholder="Unvani"
                        onChange={onChangeHandler("Unvani")}
                        value={tempModalData.Unvani}
                    />
                </Form.Group>
            </Form>
            <hr/>
            <h4>İletişim</h4>
            <Form className="row">
                <Form.Group className="col-md-6 col-xs-12">
                    <Form.Label>Cep Tel</Form.Label>
                    <Form.Control type="number"
                        placeholder="CepTel"
                        onChange={onChangeHandler("CepTel")}
                        value={tempModalData.CepTel}
                    />
                </Form.Group> 
                <Form.Group className="col-md-6 col-xs-12">
                    <Form.Label>Is Tel</Form.Label>
                    <Form.Control type="number"
                        placeholder="IsTel"
                        onChange={onChangeHandler("IsTel")}
                        value={tempModalData.IsTel}
                    />
                </Form.Group>
                <Form.Group className="col-md-6 col-xs-12">
                    <Form.Label>Dahili</Form.Label>
                    <Form.Control type="number"
                        placeholder="Dahili"
                        onChange={onChangeHandler("Dahili")}
                        value={tempModalData.Dahili}
                    />
                </Form.Group>
                <Form.Group className="col-md-12 col-xs-12">
                    <Form.Label>Mail</Form.Label>
                    <Form.Control type="text"
                        placeholder="Mail"
                        onChange={onChangeHandler("Mail")}
                        value={tempModalData.Mail}
                    />
                </Form.Group> 
            </Form>
        </>)
    }
}



export default MusteriAdayEklemeForm;
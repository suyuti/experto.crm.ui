import React from 'react';
import {  Nav } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faUsersCog,  faUsers, faBoxOpen,  faAngleDoubleRight, faAngleDoubleLeft } from '@fortawesome/free-solid-svg-icons';
import {  NavLink } from 'react-router-dom';

function LinkIC({ link, icon, text }) {
  return (
      <NavLink to={link} exact activeClassName="sidebar-active">
        <FontAwesomeIcon icon={icon} className="icon" />
        <flagment className="text">{text} </flagment>
      </NavLink>
  )
}

function SideBar({ isOpen, toggle }) {

  return (
    <div className={`sidebar ${isOpen ? "hower" : "active"}`}   >
      <Nav  >
        <LinkIC link="/" icon={faHome} text="Anasayfa" className="sidebarLink"/>
        <LinkIC link="/musteri" icon={faUsers} text="Müşteriler" />
        {/* <LinkIC link="/firma" icon={faBox} text="Firmalar" /> */}
        {/* <LinkIC link="/sektor" icon={faBoxes} text="Sektörler" /> */}
        <LinkIC link="/urunler" icon={faBoxOpen} text="Ürünler" />
        <LinkIC link="/user" icon={faUsersCog} text="Kullanıcılar" />
        

        {/* <Nav.Link eventKey="disabled" disabled>Disabled</Nav.Link>  */}
      </Nav>
      <div className="toggle">
        <FontAwesomeIcon icon={isOpen ? faAngleDoubleRight : faAngleDoubleLeft} className="icon" onClick={toggle} />
      </div>
    </div>
  );
}

var style = {


}
export default SideBar;

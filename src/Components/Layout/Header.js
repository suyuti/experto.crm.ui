import React, { useState, useEffect } from 'react';
import { Modal, Container, Navbar, Button, NavDropdown, Nav, Form, FormControl, NavItem, DropdownButton } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faEdit, faSignOutAlt, faUserShield, faSearch, faGlobeEurope, faUserAlt,
  faSpinner, faMailBulk, faPlus, faGlobe, faPhone, faMapMarked
} from '@fortawesome/free-solid-svg-icons';
import { Link, NavLink } from 'react-router-dom';

import { musteriService } from '../../Services/musteri.service'

function App({ user }) {
  const [popup, setPopup] = useState(false);
  const [musteri, setMusteri] = useState([]);
  const [select, setSelect] = useState(null);
  var subtitle = "Teklif Ver"

  useEffect(() => {
    musteriService.getAll().then(response => {
      setMusteri(response.data)
    })
  }, [])

  const logout = () => {
    localStorage.removeItem("currentUser");
    localStorage.removeItem("token");
    window.location = "/login";
  }
  return (
    <header id="header">
      <Navbar bg="danger" expand="md" >
        <Navbar.Brand href="#home"> <FontAwesomeIcon icon={faGlobeEurope} /> <span className="ml-3">EXPERTO</span> </Navbar.Brand>

        <Button variant="outline-primary" onClick={() =>{ setPopup(true);setSelect(null)}}> Teklif Ver </Button>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto row" >
            <Form className="col-xs-4 d-inline-flex">
              <React.Fragment className="row">
                <FormControl type="text" placeholder="Ara" className="mr-1 col-xs" />
                <Button variant="outline-primary" className="mr-1 col-xs-2"><FontAwesomeIcon icon={faSearch} /></Button>
              </React.Fragment>
            </Form>

            <DropdownButton className="col-xs-5" variant="dark" id="nav-dropdown" title={<span><FontAwesomeIcon icon={faUserAlt} /> {user.KullaniciAdi}</span>}>
              <NavDropdown.Item eventKey="4.1">
                <FontAwesomeIcon icon={faEdit} /> Profil
                </NavDropdown.Item>
              {/* <NavDropdown.Item eventKey="4.2"  >Another action</NavDropdown.Item>
              <NavDropdown.Item eventKey="4.3"  >Something else here</NavDropdown.Item> */}
              <NavDropdown.Divider />
              <NavDropdown.Item eventKey="4.4" onClick={logout}>
                <FontAwesomeIcon icon={faSignOutAlt} /> Çıkış
              </NavDropdown.Item>
            </DropdownButton>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Modal
        show={popup}
        onHide={() => { setPopup(false) }}
        dialogClassName="modal-90w"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            {subtitle}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body> 
          <select id="firma-il"
            className="custom-select"
            onChange={(e) => {setSelect(e.target.value) }} >
            <option hidden selected>Firmama Seçiniz</option>
            {
              musteri.map(item => {
                return (<option value={item._id}>{item.FirmaUnvani}</option>)
              })
            }
          </select>

        </Modal.Body>
        <Modal.Footer>

         {select&& <NavLink to={`/musteri/${select}/teklif`} exact className="btn btn-outline-primary" onClick={() => setPopup(false)}>
            <flagment className="text">Git </flagment>
          </NavLink>}

          <Button variant="danger" onClick={() => setPopup(false)}>İPTAL</Button>

        </Modal.Footer>
      </Modal>

    </header>

  );
}

export default App;

import React from 'react';  
import FooterComponent from './Footer';
import Router from '../../Router';
import {Container  } from 'react-bootstrap'; 
function App() {
  return (
    <div className="main-content d-flex  flex-column w-100 page" style={{    overflow: "auto" ,height: "92vh" }}>
       <div style={{flex: "1 0 auto"}}  >
          <Container fluid={true} className="container-content">
              <div className="page-box">
                <Router/> 
              </div>
          </Container>
      </div>
      <FooterComponent /> 
    </div>
  );
}

export default App;

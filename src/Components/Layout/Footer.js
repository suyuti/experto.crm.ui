import React from 'react';
function App() {
    return (
        <footer  >
            <div className="float-right" style={{paddingRight: 15}}>
                <a href="https://www.experto.com.tr">Experto </a>
                <span>&copy; 2020</span>
            </div>
        </footer>
    );
}

export default App;

import React, { Component } from 'react';
import { Form, Button, Col } from 'react-bootstrap' 

class MusteriAdayEklemeForm extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    componentDidMount() {

    }
 
    render() {
        const {onChangeHandler,tempModalData} = this.props
        return(
            <Form>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Label>Adı</Form.Label>
                  <Form.Control type="text"
                    placeholder="Ürün Adı"
                    onChange={onChangeHandler("Adi")}
                    value={tempModalData.Adi}
                  />
                </Form.Group>
              </Form.Row>

              <hr/>

              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Label>Ön Ödeme Tutarı</Form.Label>
                  <Form.Control type="number"
                    placeholder="Ön Ödeme Tutarı"
                    onChange={onChangeHandler("OnOdemeTutari")}
                    value={tempModalData.OnOdemeTutari}
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Rapor Başı Ödeme Tutarı</Form.Label>
                  <Form.Control type="number"
                    placeholder="Rapor Başı Ödeme Tutarı"
                    onChange={onChangeHandler("RaporBasiOdemeTutari")}
                    value={tempModalData.RaporBasiOdemeTutari}
                  />
                </Form.Group>
              </Form.Row>
              
              <Form.Row>
                <Form.Group as={Col}>
                    <Form.Label>Yüzde Tutarı </Form.Label>
                    <Form.Control type="number" 
                        placeholder="Yüzde Tutarı" 
                        onChange={onChangeHandler("YuzdeTutari")}
                        value={tempModalData.YuzdeTutari}
                     /> 
                </Form.Group>
                <Form.Group as={Col}>
                    <Form.Label>Sabit Tutar</Form.Label>
                    <Form.Control type="number" 
                        placeholder="Sabit Tutar" 
                        onChange={onChangeHandler("SabitTutar")}
                        value={tempModalData.SabitTutar}
                     /> 
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col}>
                    <Form.Label>Başarı Tutarı</Form.Label>
                    <Form.Control type="number" 
                        placeholder="Başarı Tutarı" 
                        onChange={onChangeHandler("BasariTutari")}
                        value={tempModalData.BasariTutari}
                     /> 
                </Form.Group>
                <Form.Group as={Col}>
                    <Form.Label>KDV Oranı (%)</Form.Label>
                    <Form.Control type="number" 
                        placeholder="KDV Oranı" 
                        onChange={onChangeHandler("KDVOrani")}
                        value={tempModalData.KDVOrani}
                     /> 
                </Form.Group>
              </Form.Row>

              <hr />
              
              <Form.Row>
                <Form.Group as={Col}>
                    <Form.Label>Aktif
                    <Form.Control type="checkbox"
                        onChange={ ()=> {   onChangeHandler("AktifMi")(!Boolean(tempModalData.AktifMi) )  }}
                        checked={tempModalData.AktifMi}
                    /> </Form.Label>
                </Form.Group>
              </Form.Row>
            </Form>        
        )
    }
}
 
  

export default MusteriAdayEklemeForm;
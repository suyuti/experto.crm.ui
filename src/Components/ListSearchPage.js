import React, { Component} from 'react'
import PageToolbar from './PageToolbar' 
import { Modal, Button } from 'react-bootstrap'
import BootstrapTable from 'react-bootstrap-table-next';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit,  faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator'; 
import { Link } from 'react-router-dom';
const { SearchBar } = Search;



export default class ListSearchPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show:props.modalOpen?props.modalOpen: false,
            tempModalData: {}, 
        }
        this.ActionButtons = this.ActionButtons.bind(this)
        this.LinkFormatter = this.LinkFormatter.bind(this)
    }
    // componentDidUpdate(prevProps){
    //     if( prevProps.modalOpen  !==  this.props.modalOpen ){
    //        this.forceUpdate();
    //         return true;
    //     }
    // }

 
 
    // componentDidMount() {
    //     setInterval(() => {
    //          this.state._data[0].Adi++
            
    //         this.setState({})
    //         console.log(this.state._data)

    //     }, 1000)
    // }
    

    handleModelEdit = (cell, row) => {
        //console.log(row.id)
        this.state.tempModalData=row;
        this.setState({ show: true })
    } 

    handleModelDelete = (cell, row) => {
        //var data = this.state.data;
        //data = data.filter(d => d.id !== row.id)
        //this.setState({data:data})
        //console.log(row.id)
        this.props.onDelete(row);
    }


    ActionButtons = (cell, row) => {
        return (
            <div>
                <button type="button" className="btn btn-outline-primary btn-sm ts-buttom" size="sm" onClick={(e) => this.handleModelEdit(cell, row)}>
                    <FontAwesomeIcon icon={faEdit} />
                </button> 
                <button type="button" className="btn btn-outline-danger btn-sm ml-2 ts-buttom" size="sm" onClick={(e) => this.handleModelDelete(cell, row)}>
                  <FontAwesomeIcon icon={faTrashAlt} />
                </button>
            </div>
        );
    }


    LinkFormatter = (cell, row, rowIndex, extraData) => {
        if (typeof (cell) === 'object') {
            // link bir nesne icin ise
            return (
                <Link to={`${extraData}/${cell._id}`} >
                    {cell.text}
                </Link>
            )
        }
        else if (`${cell}`.match(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm)) {
            // link bir url ise
            return (
                <a href={cell}>{cell}</a>
            )
        }
        else {
            // link detail icin ise
            return (
                <Link to={`${extraData}/${row._id}`} >
                    {cell}
                </Link>
            )
        }
    }

    onSave = () => { 
        
       var status=  this.props.onNew(this.state.tempModalData);
        if(status){
            this.setState({ show: false });
        }
        
    }

    onClose = () => {
        this.setState({ show: false });
        this.setState({ tempModalData: {} });
    }


    onHandleChangeModalData = (value) => e => {
        var tempData = this.state.tempModalData;
        var data;

        if(e.target===undefined){
            data=e; 
        }else if(e.target.type && e.target.type==="number"){
            data=parseInt(e.target.value);
        }else{
            data=e.target.value
        } 
        tempData[value] = data;
       // console.log(tempData);
        //console.log(e.target)
        this.setState({tempModalData: tempData})
    }

    render() {
        const { show } = this.state
        var { title, subtitle, columns, data, ModelForm, headerClasses ,onUpdate} = this.props
        var _headerClasses = headerClasses || "list-header"

        // edit buton gosterme yetkisi denetlenmeli
        if (columns.filter(c => c.text === 'Action').length === 0) {
            columns.push({ text: 'Action', dataField: '', classes: 'p-1', formatter: this.ActionButtons })
        }

        columns = columns.map(c => {
            if (c.link !== undefined) {
                c['formatter'] = this.LinkFormatter
                c['formatExtraData'] = c.link
            }
            return c;
        })

        return (
            <React.Fragment> 
            {this.state.show}
                <div className="row">
                    <div className="col">
                        <h1 className="text-left"> <small>{title}</small></h1>

                        <ToolkitProvider
                            keyField="id"
                            data={data}
                            columns={columns}
                            search
                            bootstrap4
                        >
                            {
                                _props => {
                                    
                                    return (
                                    <>
                                      <div className="all-toolbars bg-white">
                                        <PageToolbar subtitle={""}  onNew={() => { this.setState({ show: true }) }}>
                                            <SearchBar placeholder="Arama" {..._props.searchProps} />
                                        </PageToolbar>
                                      </div>
                                        <div className="all-tables container-fluid bg-white pb-3">
                                          <BootstrapTable
                                            bootstrap4
                                            headerClasses={_headerClasses}
                                            pagination={paginationFactory()}
                                            {..._props.baseProps}
                                          />
                                        </div>
                                    </>)}
                            }
                        </ToolkitProvider>
                    </div>
                </div>

                <Modal
                    show={show}
                    onHide={() => { this.setState({ show: false }) }}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="example-custom-modal-styling-title">
                          {subtitle}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ModelForm onChangeHandler={this.onHandleChangeModalData} tempModalData={this.state.tempModalData}/>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="danger" onClick={this.onClose}>İPTAL</Button>
                    {this.state.tempModalData._id? <Button onClick={() => onUpdate(this.state.tempModalData)}>GÜNCELLE</Button>
                    :<Button onClick={() => this.onSave(this.state.tempModalData)}>KAYDET</Button>
                        }
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        )
    }
}


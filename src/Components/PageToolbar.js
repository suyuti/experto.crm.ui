import React, { Component } from 'react'
import { Button, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon }  from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

export default class PageToolbar extends Component {
    render() {
        const {title, subtitle} = this.props
        return (
                <Row className="page-list-toolbar">
                    <Col className="col-md-4 col-sm-4 mt-2 text-left">
                      {this.props.children}
                    </Col>
                    <Col className="col-md text-center">
                        <h1 className=""> {title} <small>{subtitle} </small></h1>
                    </Col>
                    <Col className="col-md-3 col-sm-3 my-auto text-right">
                        <Button onClick={this.props.onNew} className="pl-3 pr-3 mr-3" style={{height: 38}}>
                          <FontAwesomeIcon icon={faPlus} /> <span className="w-100 ml-2">Yeni</span>
                        </Button>
                    </Col>
                </Row>
        )
    }
}

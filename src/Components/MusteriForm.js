import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap'
import { urunService } from '../Services/urun.service'
import { sektorService } from '../Services/sektor.service'
import { osbService } from '../Services/obs.service'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
const ilIlce = require('../il-ilce.json')

var minDate = new Date();;
var maxDate = new Date();
maxDate.setDate(maxDate.getDate() + 15);
var currentUser = JSON.parse(localStorage.getItem("currentUser"));

class MusteriAdayEklemeForm extends Component {

    state = {
        urun: [],
        selectUrun: {},
        sektor: [],
        obs: [],
        seciliIl: "",
        ilIlce: ilIlce,
    }
    constructor(props) {
        super(props);


    }
    componentDidMount() {
        urunService.getAll().then(response => {
            this.setState({ urun: response.data })
        });
        sektorService.getAll().then(response => {
            this.setState({ sektor: response.data })
        });
        osbService.getAll().then(response => {
            this.setState({ obs: response.data })
        });



    }
    componentWillMount() {

    }




    handleSelectUrun = (e) => {
        // var id = e.target.value;
        // var index = this.state.urun.findIndex((item) => item._id == id)
        // this.props.onChangeHandler("OnOdemeTutari")(this.state.urun[index].OnOdemeTutari);
        // this.props.onChangeHandler("RaporBasiOdemeTutari")(this.state.urun[index].RaporBasiOdemeTutari);
        // this.props.onChangeHandler("YuzdeTutari")(this.state.urun[index].YuzdeTutari);
        // this.props.onChangeHandler("SabitTutar")(this.state.urun[index].SabitTutar);
        // this.props.onChangeHandler("BasariTutari")(this.state.urun[index].BasariTutari);

        // this.setState({ selectUrun: this.state.urun[index] });
    }

    maxLengthCheck = (value) => {


    }

    render() {
        const { onChangeHandler, tempModalData } = this.props
        return (<>
            <Form className="row pl-4">
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Firma Ünvani
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Firma Unvani"
                            onChange={onChangeHandler("FirmaUnvani")}
                            value={tempModalData.FirmaUnvani}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Firma Markasi
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Firma Markasi"
                            onChange={onChangeHandler("FirmaMarkasi")}
                            value={tempModalData.FirmaMarkasi}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Sektör
                        </Form.Label>
                        <select className="custom-select col-sm-6" onChange={onChangeHandler("Sektor")}  >
                            <option selected>Sektör Seciniz...</option>
                            {
                                this.state.sektor.map(item => {
                                    return (<option value={item._id}>{item.Adi}</option>)
                                })
                            }
                        </select>
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Kobi Mi
                        </Form.Label>
                        <div className="col-sm-8 text-left">
                          <Form.Control
                            style={{width:"40px"}}
                            type="checkbox" 
                            placeholder="Kobi Mi"
                            onChange={onChangeHandler("KobiMi")}
                            checked={tempModalData.KobiMi=="true" || tempModalData.KobiMi==true}
                            value={tempModalData.KobiMi=="true"|| tempModalData.KobiMi==true?"false":"true"}  
                        />  
                        </div>
                        
                    </div>
                </Form.Group>

            </Form>

            <h4 className="musteri-detay-form-title">Adress</h4>
            <hr />
            <Form className="row pl-4">
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            il
                        </Form.Label>
                        <select id="firma-il" className="custom-select col-sm-6" onChange={(e) => { this.setState({ seciliIl: e.target.value }); onChangeHandler("il")(e) }} >
                            <option selected>il Seciniz...</option>
                            {
                                this.state.ilIlce.map(item => {
                                    return (<option value={item.il}>{item.il}</option>)
                                })
                            }
                        </select>
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            ilçe
                        </Form.Label>
                        <select id="firma-ilce" className="custom-select col-sm-6" onChange={onChangeHandler("ilce")} >
                            <option selected>ilçe Seciniz...</option>
                            {
                                this.state.seciliIl &&
                                this.state.ilIlce.find(i => i.il == this.state.seciliIl).ilceleri.map(item => {
                                    return (<option >{item}</option>)
                                })
                            }
                        </select>
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Adres
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Adres"
                            onChange={onChangeHandler("Adres")}
                            value={tempModalData.Adres}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            OSB
                        </Form.Label>
                        <select className="custom-select col-sm-6" onChange={onChangeHandler("OSB")}  >
                            <option selected>OSB Seciniz...</option>
                            {
                                this.state.obs.map(item => {
                                    return (<option value={item._id}>{item.Adi}</option>)
                                })
                            }
                        </select>
                    </div>
                </Form.Group>
            </Form>
            <h4 className="musteri-detay-form-title">İletişim</h4>
            <hr />
            <Form className="row pl-4">
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Web
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Web"
                            onChange={onChangeHandler("Web")}
                            value={tempModalData.Web}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Mail
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Mail"
                            onChange={onChangeHandler("Mail")}
                            value={tempModalData.Mail}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Telefon
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Telefon"
                            onChange={onChangeHandler("Telefon")}
                            value={tempModalData.Telefon}
                        />
                    </div>
                </Form.Group>
            </Form>
            <h4 className="musteri-detay-form-title">Yasal</h4>
            <hr />
            <Form className="row pl-4">
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Vergi Dairesi
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Vergi Dairesi"
                            onChange={onChangeHandler("VergiDairesi")}
                            value={tempModalData.VergiDairesi}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Vergi No
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Vergi No"
                            onChange={onChangeHandler("VergiNo")}
                            value={tempModalData.VergiNo}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            EFatura Mi
                        </Form.Label>
                        <div className="col-sm-8 text-left">
                          <Form.Control
                          style={{width:"40px"}}
                            type="checkbox" 
                            placeholder="EFatura Mi"
                            onChange={onChangeHandler("EFaturaMi")}
                            value={tempModalData.EFaturaMi=="true" || tempModalData.EFaturaMi==true?"false":"true"}  
                            checked={tempModalData.EFaturaMi=="true" || tempModalData.EFaturaMi==true}
                        />  
                        </div>
                        
                    </div>
                </Form.Group>
            </Form>
            <h4 className="musteri-detay-form-title">Mali</h4>
            <hr />
            <Form className="row pl-4">
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Ciro
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Ciro"
                            onChange={onChangeHandler("Ciro")}
                            value={tempModalData.Ciro}
                        />
                    </div>
                </Form.Group>
                <Form.Group className="col-sm-12">
                    <div className="row">
                        <Form.Label
                            className="col-sm-4 ">
                            Calisan Sayisi
                        </Form.Label>
                        <Form.Control
                            type="text"
                            className="col-sm-8"
                            placeholder="Calisan Sayisi"
                            onChange={onChangeHandler("CalisanSayisi")}
                            value={tempModalData.CalisanSayisi}
                        />
                    </div>
                </Form.Group>
            </Form>
        </>)
    }
}



export default MusteriAdayEklemeForm;